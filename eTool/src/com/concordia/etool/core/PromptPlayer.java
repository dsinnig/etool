package com.concordia.etool.core;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

import com.concordia.etool.core.helpers.EToolApplication;


public class PromptPlayer implements OnPromptCompletion{

	PlayAudio playAudio;

	private String[] prompts;
	private boolean isPlaying;
	public boolean isPlaying() {
		return isPlaying;
	}

	private boolean isComplete;
	public boolean isComplete() {
		return isComplete;
	}

	public void setComplete(boolean isComplete) {
		this.isComplete = isComplete;
	}

	private int promptsCount;

	OnPromptCompletion promptCompletionListener;

	private static final PromptPlayer instance = new PromptPlayer();

	private PromptPlayer() {

		isPlaying = false;
		isComplete = true;

		playAudio = EToolApplication.getInstance(null).getAudioPlayer();

		playAudio.getMediaPlayer().setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				isPlaying = false;
				playAudio.resetAudio();
				playNext();
			}
		});

	}

	public void setOnPromptCompleteListener(OnPromptCompletion listener)
	{
		this.promptCompletionListener = listener;
	}

	public static PromptPlayer getInstance() {
		return instance;
	}

	public void playPrompt(String prompt)
	{
		promptsCount = 1;
		prompts[0] = prompt;
		playNext();
		isPlaying = true;
		isComplete = false;
	}

	public void playPrompts(String[] prompts)
	{

		if(prompts!=null) {

			this.prompts = prompts;

			promptsCount = prompts.length;

			isComplete = false;
			playNext();

		}
	}

	public void pausePrompt()
	{
		playAudio.playPauseToggle();
		if(isPlaying)
			isPlaying = false;
		else
			isPlaying = true;
	}

	public void stopPrompt()
	{
		if (isPlaying) 
			pausePrompt();
		
		isPlaying = false;
		playAudio.stopAudio();
		playAudio.resetAudio();
	}

	public void releasePrompts() 
	{
		isPlaying = false;

		playAudio.releaseAudio();
	}

	public void playNext()
	{
		if(isPlaying)
			stopPrompt();

		if(promptsCount>0) {
			promptsCount--;

			String path = EToolApplication.getDirectory("navDir");
			path += prompts[prompts.length - promptsCount - 1];

			playAudio.playAudio(path);
			isPlaying = true;

		} 

		else if(promptsCount==0 && !isComplete)
		{
			onComplete(instance);
		}

	}

	public void playPrev()
	{
		if(promptsCount>1)
			playAudio.playAudio(prompts[--promptsCount]);
	}

	@Override
	public void onComplete(PromptPlayer promptPlayer) {
		if(promptCompletionListener!=null)
		{
			if(promptsCount==0)
			{
				isComplete = true;
				promptCompletionListener.onComplete(promptPlayer);
				
			}
		}

	}

}
