package com.concordia.etool.core.helpers;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.concordia.etool.R;
import com.concordia.etool.core.helpers.ImageLoader.ImageLoadListener;

public class LazyImageAdapter extends BaseAdapter implements ImageLoadListener {

	private static final int PROGRESSBARINDEX = 0;
	private static final int IMAGEVIEWINDEX = 1;

	private Context mContext = null;
	private Handler mHandler;
	private ImageLoader mImageLoader = null;
	private File mDirectory;
	private static ArrayList<ImageTagHolder> imgList;
	
	private int thumbnailSize;
	private LayoutInflater inflater;

	/**
	 * Lazy loading image adapter
	 * @param aContext
	 * @param lClickListener click listener to attach to each item
	 * @param lPath the path where the images are located
	 * @throws Exception when path can't be read from or is not a valid directory
	 */
	public LazyImageAdapter(Context aContext, String lPath)
	{

		mContext = aContext;
		
		mDirectory = new File(lPath);
		ImageTagHolder.dirPath = EToolApplication.getDirectory("libDir");
		
		if(EToolApplication.Screenheight>EToolApplication.Screenwidth) {
			
			thumbnailSize = (int) Math.round(EToolApplication.Screenwidth / 4.0 );
		}
		
		else {
			thumbnailSize = (int) (EToolApplication.Screenheight/4);
		}
		
		inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		imgList = new ArrayList<ImageTagHolder>();

		File[] storiesList = mDirectory.listFiles();

		Arrays.sort(storiesList,fileNameComparator);

		for(File story : storiesList)
		{
			if(story.isDirectory())
			{

				File[] dispFiles = story.listFiles(new FileFilter() {

					@Override
					public boolean accept(File dir) {
						return (dir.getName().toLowerCase().matches(".*jpg")||dir.getName().toLowerCase().matches(".*png"));
					}
				});

				Arrays.sort(dispFiles);

				ImageTagHolder imageHolder = new ImageTagHolder();
				imageHolder.dir = story.getName();
				if(dispFiles.length>0)
					imageHolder.img = dispFiles[0].getName();
				else
					imageHolder.img = null;

				imgList.add(imageHolder);
			}
		}

		mImageLoader = new ImageLoader(this);
		mImageLoader.start();
		mHandler = new Handler();

	}

	Comparator<? super File> fileNameComparator = new Comparator<File>(){
		@Override
		public int compare(File file1, File file2) {
			return Integer.valueOf(file1.getName()).compareTo(Integer.valueOf(file2.getName()));
		} 
	};

	@Override
	protected void finalize() throws Throwable {
		super.finalize();

		// stop the thread we started
		mImageLoader.stopThread();
		imgList = null;
	}

	@Override
	public int getCount() {
		return imgList.size();
	}

	@Override
	public Object getItem(int aPosition) {
		ImageTagHolder result = null;
		if(aPosition<imgList.size())
			result = imgList.get(aPosition);
		return result;
	}

	@Override
	public long getItemId(int arg0) {
		return Long.parseLong(imgList.get(arg0).dir);
	}

	@Override
	public View getView(final int aPosition, View aConvertView, ViewGroup parent) {

		View itemView = aConvertView;
		ViewSwitcher lViewSwitcher = null;
		TextView text = null;

		ImageView lImage = new ImageView(mContext);
		ProgressBar lProgress = new ProgressBar(mContext);
		
		if(aConvertView == null)
		{
			itemView = (FrameLayout) inflater.inflate(R.layout.library_item, parent, false);
			lViewSwitcher = (ViewSwitcher) itemView.findViewById(R.id.imageLibItem);
			lViewSwitcher.setLayoutParams(new FrameLayout.LayoutParams(thumbnailSize, thumbnailSize));
			lProgress.setLayoutParams(new ViewSwitcher.LayoutParams(thumbnailSize, thumbnailSize));
			lImage.setLayoutParams(new ViewSwitcher.LayoutParams(thumbnailSize, thumbnailSize));
			
			lViewSwitcher.addView(lProgress);
			lViewSwitcher.addView(lImage);
			
			ImageTagHolder lNewTag = (ImageTagHolder) getItem(aPosition);
			String lPath = ImageTagHolder.dirPath + lNewTag.dir + File.separator +lNewTag.img;
			lNewTag.fullPath = lPath;

		}

		ImageTagHolder imgHolder = (ImageTagHolder) getItem(aPosition);
		
		text = (TextView)itemView.findViewById(R.id.textLibItem);
		text.setText(imgHolder.dir);

		// Grab the image view
		// Have the progress bar display
		// Then queue the image loading
		
		if(lViewSwitcher!=null)
		{
			lViewSwitcher.setDisplayedChild(PROGRESSBARINDEX);

			//first image file is chosen as a display thumbnail (alphabetically)
			if (imgHolder.img!=null)
			{
				ImageView imgView = (ImageView)lViewSwitcher.getChildAt(IMAGEVIEWINDEX);
				imgView.setScaleType(ScaleType.CENTER);
				mImageLoader.queueImageLoad(imgHolder.fullPath, imgView, lViewSwitcher);
			}
			
			// No image is found associated with the story
			else
			{
				lImage.setBackgroundColor(Color.rgb(255, 255, 255));
				lImage.setImageResource(R.drawable.default_user);
				lViewSwitcher.setDisplayedChild(IMAGEVIEWINDEX);
			}
		}

		return itemView;
	}

	@Override
	public void handleImageLoaded(
			final ViewSwitcher aViewSwitcher,
			final ImageView aImageView, 
			final Bitmap aBitmap) {

		// The enqueue the following in the UI thread
		mHandler.post(new Runnable() {
			@Override
			public void run() {

				// set the bitmap in the ImageView
				aImageView.setImageBitmap(aBitmap);

				// explicitly tell the view switcher to show the second view
				if(aViewSwitcher!=null)
					aViewSwitcher.setDisplayedChild(IMAGEVIEWINDEX);

				deAllocatae(aBitmap);
			}
		});

	}

	private void deAllocatae(Bitmap b)
	{
		b = null;
		System.gc();
	}

	public static String getDirForImg(int pos)
	{
		if(pos<imgList.size())
			return imgList.get(pos).dir;
		return null;
	}
}


class ImageTagHolder {

	static String dirPath;
	String dir;
	String img;
	String fullPath;
}
