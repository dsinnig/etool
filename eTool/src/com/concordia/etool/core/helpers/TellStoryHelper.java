package com.concordia.etool.core.helpers;

import java.util.ArrayList;

public class TellStoryHelper {

	private ArrayList<String> question;
	private ArrayList<String> response;

	public TellStoryHelper(ArrayList<String> question) {
		this.question = question;
	}

	public TellStoryHelper(ArrayList<String> question, ArrayList<String> response)
	{
		this.question = question;
		this.response = response;
	}

	public boolean addResponse(String currentResponce)
	{
		boolean result = false;

		if(response.size()<=question.size())
		{
			response.add(currentResponce);
			result = true;
		}
		
		return result;
	}
}
