  package com.concordia.etool.core.helpers;


public class PlayerDataType {
	
	public static final int  picture_type = 1;
	public static final int audio_type = 2;
	public static final int video_type = 3;
	
	private String data;
	private int type;
	
	public PlayerDataType(String data, int type) {
		super();
		this.setData(data);
		this.setType(type);
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	
}
