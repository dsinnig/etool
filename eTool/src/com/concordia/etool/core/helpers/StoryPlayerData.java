package com.concordia.etool.core.helpers;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class StoryPlayerData {

	ArrayList<PlayerDataType> data;

	public StoryPlayerData(String pathToStory)
	{
		ArrayList<String> tempData = getFileList(pathToStory);
		setData(buildIntroData(tempData));
	}

	private ArrayList<String> getFileList(String path)
	{
		File storyDir = new File(path);
		String[] fileList = null;
		if(storyDir.isDirectory())
		{
			fileList = storyDir.list();
		}

		if(fileList!=null)
		{
			Arrays.sort(fileList, fileNameComparator);
		}
		ArrayList<String> toReturn = new ArrayList<String>();

		for(String file : fileList)
		{
			if(file.toLowerCase().contains("wav") || file.toLowerCase().contains("jpg") || file.toLowerCase().contains("mp4") || file.toLowerCase().contains("3gp"))
				toReturn.add(file);
		}

		return toReturn;
	}

	Comparator<? super String> fileNameComparator = new Comparator<String>(){
		@Override
		public int compare(String file1, String file2) {
			return String.valueOf(file1).compareTo(file2);
		} 
	};

	private ArrayList<PlayerDataType> buildIntroData(ArrayList<String> data)
	{
		ArrayList<PlayerDataType> toReturn = new ArrayList<PlayerDataType>();
		PlayerDataType curData;
		for(int i= 0; i<data.size(); i++)
		{
			String curString = data.get(i);

			if(curString.toLowerCase().contains(".3gp") || curString.toLowerCase().contains(".wav"))
				curData = new PlayerDataType(curString, PlayerDataType.audio_type);
			else if (curString.toLowerCase().contains(".jpg") || curString.toLowerCase().contains(".png"))
				curData = new PlayerDataType(curString, PlayerDataType.picture_type);
			else
				curData = new PlayerDataType(curString, PlayerDataType.video_type);

			toReturn.add(curData);
		}

		return toReturn;
	}

	public ArrayList<PlayerDataType> getData() {
		return data;
	}

	public void setData(ArrayList<PlayerDataType> data) {
		this.data = data;
	}

	private int getMediaType(int itemAt)
	{
		return data.get(itemAt).getType();
	}

	public String getNextMedia(int itemAt, int mediaType)
	{
		if(itemAt<data.size())
		{
			for(int i = itemAt; i< data.size(); i++)
			{
				if(getMediaType(i) == mediaType)
					return data.get(i).getData();
			}
		}

		return null;
	}

	public String getNextMedia(int itemAt)
	{
		if(itemAt<data.size()-1)
			return data.get(itemAt+1).getData();
		return null;
	}

	public int getNextMediaType(int itemAt)
	{
		if(itemAt<data.size())
			return data.get(itemAt+1).getType();
		return 0;
	}

	public ArrayList<String> getAllMediaOfType(int type)
	{
		ArrayList<String> tempMediaList = new ArrayList<String>();
		for(PlayerDataType curData : data)
		{
			if(curData.getType() == type)
				tempMediaList.add(curData.getData());
		}

		return tempMediaList;
	}

	public int getIndex(String dataToFind)
	{
		int i = 0;
		while(!data.get(i).getData().equalsIgnoreCase(dataToFind))
		{
			if(i==data.size())
			{
				i = 0;
				break;
			}
			i++;
		}
		
		return i;
	}
	
	public boolean isLastMedia(String mediaFile)
	{
		return (getIndex(mediaFile)<data.size());
	}
	
}
