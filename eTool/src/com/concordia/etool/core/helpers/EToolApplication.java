package com.concordia.etool.core.helpers;

import java.io.File;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Point;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Environment;
import android.view.Display;
import android.view.WindowManager;

import com.concordia.etool.core.PlayAudio;
import com.concordia.etool.core.PromptPlayer;
import com.concordia.etool.core.RecordAudio;
import com.concordia.etool.core.TakePicture;
import com.concordia.etool.forFuture.PlayVideo;
import com.concordia.etool.forFuture.RecordVideo;
import com.concordia.etool.graph.DialogGraph;
import com.concordia.etool.graph.Node;

public class EToolApplication extends Application {

	private static EToolApplication instance = null;
	public static boolean isInitialized = false;
	public static boolean isReturning = false;

	//Application static directory
	public static String mainDirectory; 
	public static String libDirectory;
	public static String promptsDirectory;
	public static String questionsPromptsDirectory;
	public static String navigationPromptsDirectory;
	public static String introductionPromptsDirectory;

	//Application dynamic directory
	public static String currentLibDirectory;
	public static String currentQuestionsPromptsDirectory;
	public static String currentNavigationPromptsDirectory;
	public static String currentIntroductionPromptsDirectory;

	private static Context applicationContext;
	
	//Dialog Graph
	public static DialogGraph dialogGraph;
	public static Node currentNode;
	
	public static double Screenwidth = 0;  
	public static double Screenheight = 0;
	public static double AspectRatio ;

	//all media controls
	private PromptPlayer promptPlayer;
	private RecordAudio audioRecorder;
	private RecordVideo videoRecorder;
	private TakePicture takePicture;
	private PlayAudio audioPlayer;
	private PlayVideo videoPlayer;

	private EToolApplication() {
		try{
			promptPlayer = PromptPlayer.getInstance();
			audioRecorder = RecordAudio.getInstance();
			videoRecorder = RecordVideo.getInstance();
			takePicture = TakePicture.getInstance();
			audioPlayer = PlayAudio.getInstance();
			videoPlayer = PlayVideo.getInstance();

			initializeEtool();
			
			initializeScreen();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	private void initializeScreen() {
		
		Point size = new Point();
		WindowManager w = (WindowManager) applicationContext.getSystemService(Context.WINDOW_SERVICE);

		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
		      w.getDefaultDisplay().getSize(size);
		      Screenwidth = size.x;
		      Screenheight = size.y; 
		    }else{
		      Display d = w.getDefaultDisplay(); 
		      Screenwidth = d.getWidth();
		      Screenheight = d.getHeight(); 
		    }

		if(Screenheight>Screenwidth) {
			 AspectRatio = Screenwidth/Screenheight; 
		}
		else {
			AspectRatio = Screenheight/Screenwidth;
		}
	}

	private void initializeEtool()
	{
		//static directory initialization
		mainDirectory = Environment.getExternalStorageDirectory()+"/etool";
		libDirectory = "/library";
		promptsDirectory = "/prompts";
		questionsPromptsDirectory = "/questions";
		navigationPromptsDirectory = "/navigation";
		introductionPromptsDirectory = "/introduction";

		//dynamic directory initialization
		currentLibDirectory = "/farmers English/";
		currentNavigationPromptsDirectory = "/default/";
		currentQuestionsPromptsDirectory = "/farmers English/";
		currentIntroductionPromptsDirectory = "/farmers English/";

		//set the number of stories in the library
		initializeStories();

		isInitialized = true;
	}

	public static boolean initializeStories()
	{
		File storyLib = new File(mainDirectory + libDirectory + currentLibDirectory);
		if(!storyLib.exists())
			storyLib.mkdirs();

		Story.storyCount = storyLib.list().length;

		return false;
	}

	public static EToolApplication getInstance(Context ctx) {
		
		if(applicationContext == null)
			applicationContext = ctx;
		
		if(instance == null)
			instance = new EToolApplication();
		return instance;
	}

	public static void incrementNode(Node n)
	{
		if(n == null)
		{
			currentNode = dialogGraph.getFirst();
			return;
		}

		else{

			Node tempNode = dialogGraph.getNextNode(n);

			if(tempNode!=null)
				currentNode = tempNode;
		}

	}

	public static void decrementNode(Node n)
	{
		if(n==null)
			currentNode = null;
		else
		{
			Node tempNode = dialogGraph.getPrevNode(n);
			if(tempNode!=null)
				currentNode = tempNode;
		}
	}

	public static int findImageRotation(String fileName)
	{
		int imgRotation = 0;
		try {
			ExifInterface exifInf = new ExifInterface(fileName);
			if(exifInf!=null)
			{
				int rotation = exifInf.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
				if(rotation == ExifInterface.ORIENTATION_ROTATE_90)
					imgRotation = 90;
				else if(rotation == ExifInterface.ORIENTATION_ROTATE_180)
					imgRotation = 90;
				else if(rotation == ExifInterface.ORIENTATION_ROTATE_270)
					imgRotation = 270;
				else if (rotation == ExifInterface.ORIENTATION_NORMAL)
					imgRotation = 270;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return imgRotation;
	}

	public static Bitmap rotateBitmap(Bitmap toRotate, int deg)
	{
		Matrix matrix = new Matrix();
		matrix.postRotate(deg);
		Bitmap rotated = Bitmap.createBitmap(toRotate, 0, 0, 
				toRotate.getWidth(), toRotate.getHeight(), 
				matrix, true);

		return rotated;
	}

	public static String getDirectory(String getDir)
	{
		if(getDir.equalsIgnoreCase("libDir"))
			return mainDirectory + libDirectory + currentLibDirectory;
		else if (getDir.equalsIgnoreCase("navDir"))
			return mainDirectory + promptsDirectory+ navigationPromptsDirectory+ currentNavigationPromptsDirectory;
		else if (getDir.equalsIgnoreCase("introDir"))
			return mainDirectory + promptsDirectory+ introductionPromptsDirectory+ currentIntroductionPromptsDirectory;
		else if (getDir.equalsIgnoreCase("questionsDir"))
			return mainDirectory + promptsDirectory+questionsPromptsDirectory+currentQuestionsPromptsDirectory;
		else if (getDir.equalsIgnoreCase("mainNavDir"))
			return mainDirectory + promptsDirectory+ navigationPromptsDirectory;
		else if (getDir.equalsIgnoreCase("mainIntroDir"))
			return mainDirectory + promptsDirectory+ introductionPromptsDirectory;
		else if (getDir.equalsIgnoreCase("mainQuestionsDir"))
			return mainDirectory + promptsDirectory+questionsPromptsDirectory;
		else if (getDir.equalsIgnoreCase("mainLibDir"))
			return mainDirectory + libDirectory;
		else 
			return null;

	}


	public static void setHandler()
	{

	}


	/**
	 * @return the promptPlayer
	 */
	public PromptPlayer getPromptPlayer() {
		return promptPlayer;
	}
	/**
	 * @param promptPlayer the promptPlayer to set
	 */
	public void setPromptPlayer(PromptPlayer promptPlayer) {
		this.promptPlayer = promptPlayer;
	}
	/**
	 * @return the audioRecorder
	 */
	public RecordAudio getAudioRecorder() {
		return audioRecorder;
	}
	/**
	 * @param audioRecorder the audioRecorder to set
	 */
	public void setAudioRecorder(RecordAudio audioRecorder) {
		this.audioRecorder = audioRecorder;
	}
	/**
	 * @return the videoRecorder
	 */
	public RecordVideo getVideoRecorder() {
		return videoRecorder;
	}
	/**
	 * @param videoRecorder the videoRecorder to set
	 */
	public void setVideoRecorder(RecordVideo videoRecorder) {
		this.videoRecorder = videoRecorder;
	}
	/**
	 * @return the takePicture
	 */
	public TakePicture getTakePicture() {
		return takePicture;
	}
	/**
	 * @param takePicture the takePicture to set
	 */
	public void setTakePicture(TakePicture takePicture) {
		this.takePicture = takePicture;
	}
	/**
	 * @return the audioPlayer
	 */
	public PlayAudio getAudioPlayer() {
		return audioPlayer;
	}
	/**
	 * @param audioPlayer the audioPlayer to set
	 */
	public void setAudioPlayer(PlayAudio audioPlayer) {
		this.audioPlayer = audioPlayer;
	}
	/**
	 * @return the videoPlayer
	 */
	public PlayVideo getVideoPlayer() {
		return videoPlayer;
	}
	/**
	 * @param videoPlayer the videoPlayer to set
	 */
	public void setVideoPlayer(PlayVideo videoPlayer) {
		this.videoPlayer = videoPlayer;
	}

}
