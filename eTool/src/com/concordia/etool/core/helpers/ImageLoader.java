package com.concordia.etool.core.helpers;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

public class ImageLoader extends Thread {

	public interface ImageLoadListener {

		void handleImageLoaded(ViewSwitcher aViewSwitcher, ImageView aImageView, Bitmap aBitmap);
	}

	private static final String TAG = ImageLoader.class.getSimpleName();
	ImageLoadListener mListener = null;
	private Handler handler;

	/**
	 * Image loader takes an object that extends ImageLoadListener
	 * @param lListener
	 */
	ImageLoader(ImageLoadListener lListener){
		mListener = lListener;
	}

	@Override
	public void run() {
		try {

			// preparing a looper on current thread			
			// the current thread is being detected implicitly
			Looper.prepare();

			// Looper gets attached to the current thread by default
			handler = new Handler();

			Looper.loop();
			// Thread will start

		} catch (Throwable t) {
			Log.e(TAG, "ImageLoader halted due to a error: ", t);
		} 
	}

	/**
	 * Method stops the looper and thus the thread
	 */
	public synchronized void stopThread() {

		// Use the handler to schedule a quit on the looper
		handler.post(new Runnable() {

			@Override
			public void run() {
				// This runs on the ImageLoader thread
				Log.i(TAG, "DownloadThread loop quitting by request");

				Looper.myLooper().quit();
			}
		});
	}

	/**
	 * Method queues the image at path to load
	 * Note that the actual loading takes place in the UI thread
	 * the ImageView and ViewSwitcher are just references for the
	 * UI thread.
	 * @param aPath      - Path where the bitmap is located to load
	 * @param aImageView - The ImageView the UI thread will load 
	 * @param aViewSwitcher - The ViewSwitcher that needs to display the imageview
	 */
	public synchronized void queueImageLoad(
			final String aPath, 
			final ImageView aImageView, 
			final ViewSwitcher aViewSwitcher) {

		// Wrap DownloadTask into another Runnable to track the statistics
		handler.post(new Runnable() {
			@Override
			public void run() {
				try {

					synchronized (aImageView){
						// make sure this thread is the only one performing activities on
						// this imageview
						BitmapFactory.Options lOptions = new BitmapFactory.Options();
						lOptions.inSampleSize = 1;
						Bitmap unScaledBitmap = decodeFile(aPath, 150, 150, ScalingLogic.FIT);
						Bitmap scaledBitmap = createScaledBitmap(unScaledBitmap, 150, 150, ScalingLogic.CROP);
						scaledBitmap = EToolApplication.rotateBitmap(scaledBitmap, EToolApplication.findImageRotation(aPath));
						//aImageView.setImageBitmap(lBitmap);

						// Load the image here
						signalUI(aViewSwitcher, aImageView, scaledBitmap);
					}
				} 
				catch(Exception e){
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Method is called when the bitmap is loaded.  The UI thread adds the bitmap to the imageview.
	 * @param aViewSwitcher - The ViewSwitcher that needs to display the imageview
	 * @param aImageView - The ImageView the UI thread will load 
	 * @param aImage - The Bitmap that gets loaded into the ImageView
	 */
	private void signalUI(
			ViewSwitcher aViewSwitcher, 
			ImageView aImageView, 
			Bitmap aImage){

		if(mListener != null){
			// we have an object that implements ImageLoadListener

			mListener.handleImageLoaded(aViewSwitcher, aImageView, aImage);
		}
	}

	public static enum ScalingLogic {
		CROP, FIT
	}

	public static Bitmap createScaledBitmap(Bitmap unscaledBitmap, int dstWidth, int dstHeight, ScalingLogic scalingLogic) {

		Rect srcRect = calculateSrcRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(), dstWidth, dstHeight, scalingLogic);

		Rect dstRect = calculateDstRect(unscaledBitmap.getWidth(), unscaledBitmap.getHeight(), dstWidth, dstHeight, scalingLogic);

		Bitmap scaledBitmap = Bitmap.createBitmap(dstRect.width(), dstRect.height(), Config.ARGB_8888);

		Canvas canvas = new Canvas(scaledBitmap);

		canvas.drawBitmap(unscaledBitmap, srcRect, dstRect, new Paint(Paint.FILTER_BITMAP_FLAG));

		return scaledBitmap;

	}

	public static Rect calculateSrcRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight, ScalingLogic scalingLogic) {

		if (scalingLogic == ScalingLogic.CROP) {

			final float srcAspect = (float)srcWidth / (float)srcHeight;

			final float dstAspect = (float)dstWidth / (float)dstHeight;

			if (srcAspect > dstAspect) {

				final int srcRectWidth = (int)(srcHeight * dstAspect);

				final int srcRectLeft = (srcWidth - srcRectWidth) / 2;

				return new Rect(srcRectLeft, 0, srcRectLeft + srcRectWidth, srcHeight);

			} else {

				final int srcRectHeight = (int)(srcWidth / dstAspect);

				final int scrRectTop = (int)(srcHeight - srcRectHeight) / 2;

				return new Rect(0, scrRectTop, srcWidth, scrRectTop + srcRectHeight);

			}

		} else {

			return new Rect(0, 0, srcWidth, srcHeight);

		}

	}

	public static Rect calculateDstRect(int srcWidth, int srcHeight, int dstWidth, int dstHeight, ScalingLogic scalingLogic) {

		if (scalingLogic == ScalingLogic.FIT) {

			final float srcAspect = (float)srcWidth / (float)srcHeight;

			final float dstAspect = (float)dstWidth / (float)dstHeight;

			if (srcAspect > dstAspect) {

				return new Rect(0, 0, dstWidth, (int)(dstWidth / srcAspect));

			} else {

				return new Rect(0, 0, (int)(dstHeight * srcAspect), dstHeight);

			}

		} else {

			return new Rect(0, 0, dstWidth, dstHeight);

		}

	}

	public static Bitmap decodeFile(String pathName, int dstWidth, int dstHeight, ScalingLogic scalingLogic) {

		Options options = new Options();

		options.inJustDecodeBounds = true;

		BitmapFactory.decodeFile(pathName, options);

		options.inJustDecodeBounds = false;

		options.inSampleSize = calculateSampleSize(options.outWidth, options.outHeight, dstWidth, dstHeight, scalingLogic);

		Bitmap unscaledBitmap = BitmapFactory.decodeFile(pathName, options);

		return unscaledBitmap;

	}

	public static int calculateSampleSize(int srcWidth, int srcHeight, int dstWidth, int dstHeight, ScalingLogic scalingLogic) {

		if (scalingLogic == ScalingLogic.FIT) {

			final float srcAspect = (float)srcWidth / (float)srcHeight;

			final float dstAspect = (float)dstWidth / (float)dstHeight;

			if (srcAspect > dstAspect) {

				return srcWidth / dstWidth;

			} else {

				return srcHeight / dstHeight;

			}

		} else {

			final float srcAspect = (float)srcWidth / (float)srcHeight;

			final float dstAspect = (float)dstWidth / (float)dstHeight;

			if (srcAspect > dstAspect) {

				return srcHeight / dstHeight;

			} else {

				return srcWidth / dstWidth;

			}

		}

	}

}
