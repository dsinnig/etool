package com.concordia.etool.core.helpers;

import java.io.File;

import android.os.Environment;

/**
 * @author Ranjith
 *
 */
public interface StorageInterface{
	
	public static final String eToolMainDir = Environment.getExternalStorageDirectory().getAbsolutePath()+"/etool";
	public static final String eToolPromptsDir = "/prompts";
	public static final String eToolStoriesDir = "/library/farmers in English/";
	public static final String eToolNavigationDir = "/navigation/default/";
	public static final String eToolQuestionsDir = "/questions/farmers in English/";
	public static final String eToolIntroDir = "/introduction/farmers in English/";
	
	public File saveMedia(String fileName);

}
