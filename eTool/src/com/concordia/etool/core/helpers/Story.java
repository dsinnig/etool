package com.concordia.etool.core.helpers;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

public class Story {

	public static int storyCount;

	private int StoryId;
	private String imageFile;
	private ArrayList<String> audioStories;
	private String videoStory;

	public Story(int storyId) {

		if(storyId>0)
		{
			this.setStoryId(storyId);
			String currentStory = Integer.toString(storyId);
			String storyPath = EToolApplication.getDirectory("libDir") + currentStory;
			File f = new File(storyPath);

			if(f.exists() && f.isDirectory())
			{
				String tempPath = storyPath+"/"+ currentStory + ".jpg";
				f = new File(tempPath);
				if(f.exists())
					setImageFile(tempPath);

				String tempVideoPath = storyPath +"/" + currentStory + ".mp4";
				f = new File(tempVideoPath);
				if(f.exists())
					setVideoStory(tempVideoPath);

				EtoolFileFiler filter = new EtoolFileFiler("3gp");
				f = new File(storyPath);
				File[] audioFile = f.listFiles(filter);

				if(audioFile.length > 0)
				{
					audioStories = new ArrayList<String>();
					
					for(int i=0; i<audioFile.length; i++)
						audioStories.add(audioFile[i].getAbsolutePath());
				}
			}

			else
			{
				f.mkdirs();
				setImageFile(null);
				audioStories = null;
				setVideoStory(null);

			}

		}

	}
	
	public static ArrayList<String> findStoryImages(File storyFolder)
	{
		if(!storyFolder.exists() || !storyFolder.isDirectory())
			return null;
		else
		{
			ArrayList<String> imageFiles = new ArrayList<String>();
			
			for(File story : storyFolder.listFiles())
			{
				String[] dispFiles = story.list(new FilenameFilter() {
					
					@Override
					public boolean accept(File dir, String filename) {
						return filename.matches("1*.jpg");
					}
				});
				
				if(dispFiles.length>0)
					imageFiles.add(story.getAbsolutePath()+File.separator+dispFiles[0]);
			}
			
			return imageFiles;
		}
	}
	
	public int createNewStory()
	{
		Story tempNewStory = new Story(storyCount + 1);
		incrementStories();
		return tempNewStory.getStoryId();
	}
	
	public static void incrementStories()
	{
		storyCount++;
	}

	/**
	 * @return the storyId
	 */
	public int getStoryId() {
		return StoryId;
	}

	/**
	 * @param storyId the storyId to set
	 */
	public void setStoryId(int storyId) {
		StoryId = storyId;
	}

	/**
	 * @return the imageFile
	 */
	public String getImageFile() {
		return imageFile;
	}

	/**
	 * @param imageFile the imageFile to set
	 */
	public void setImageFile(String imageFile) {
		this.imageFile = imageFile;
	}

	/**
	 * @return the videoStory
	 */
	public String getVideoStory() {
		return videoStory;
	}

	/**
	 * @param videoStory the videoStory to set
	 */
	public void setVideoStory(String videoStory) {
		this.videoStory = videoStory;
	}
	
}
