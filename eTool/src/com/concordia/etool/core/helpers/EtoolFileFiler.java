package com.concordia.etool.core.helpers;

import java.io.File;
import java.io.FileFilter;

public class EtoolFileFiler implements FileFilter {

	String filterExt;
	
	public EtoolFileFiler(String ext)
	{
		filterExt = ext;
	}
	
	@Override
	public boolean accept(File pathname) {
		
		if(pathname.getName().toLowerCase().endsWith(filterExt))
			return true;
		return false;
	}

}
