package com.concordia.etool.core.helpers;

import java.util.ArrayList;

public class PlayStoryHelper {

	private ArrayList<String> stories;
	private ArrayList<String> pics;
	private String video;

	public PlayStoryHelper (ArrayList<String> stories)
	{
		this.setStories(stories);
		this.pics = new ArrayList<String>();
	}

	public PlayStoryHelper(ArrayList<String> stories, ArrayList<String> pics,
			String video) {
		super();
		this.stories = stories;
		this.pics = pics;
		this.setVideo(video);
	}

	public String getNextStory(int pos)
	{
		if( pos >= stories.size())
			return null;
		return stories.get(pos+1);
	}

	public String getNextPic(int pos)
	{
		if( pos >= pics.size())
			return null;
		return pics.get(pos+1);
	}

	/**
	 * @return the stories
	 */
	public ArrayList<String> getStories() {
		return stories;
	}

	/**
	 * @param stories the stories to set
	 */
	public void setStories(ArrayList<String> stories) {
		this.stories = stories;
	}

	/**
	 * @return the video
	 */
	public String getVideo() {
		return video;
	}

	/**
	 * @param video the video to set
	 */
	public void setVideo(String video) {
		this.video = video;
	}

}
