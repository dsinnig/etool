package com.concordia.etool.core;

import java.io.File;

import com.concordia.etool.core.helpers.StorageInterface;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Environment;

public class TakePicture implements StorageInterface{

	private static final TakePicture instance = new TakePicture();

	private TakePicture() {
	}

	public static TakePicture getInstance() {
		return instance;
	}
	
	public Bitmap getPicture(Intent snappedPic)
	{
		Bitmap imageBitmap = null;

		if(snappedPic!=null)
		{
			Bundle extras = snappedPic.getExtras();
			imageBitmap = (Bitmap) extras.get("data");
		}

		return imageBitmap;
	}
	
	/**
	 * @param sampleBitmap
	 * @return Black and White Bitmap image
	 * @see "http://android-code-space.blogspot.ca/2010/08/convert-image-to-black-and-white.html"
	 * 
	 */
	public Bitmap ConvertToBlackAndWhite(Bitmap sampleBitmap){
		ColorMatrix bwMatrix =new ColorMatrix();
		bwMatrix.setSaturation(0);
		final ColorMatrixColorFilter colorFilter= new ColorMatrixColorFilter(bwMatrix);
		Bitmap rBitmap = sampleBitmap.copy(Bitmap.Config.ARGB_8888, true);
		Paint paint=new Paint();
		paint.setColorFilter(colorFilter);
		Canvas myCanvas =new Canvas(rBitmap);
		myCanvas.drawBitmap(rBitmap, 0, 0, paint);
		return rBitmap;
	}

	@Override
	public File saveMedia(String fileName) {
		String pathToSave = Environment.getExternalStorageState();
		 pathToSave += eToolMainDir + eToolStoriesDir ;
		File picToSave = new File(pathToSave, fileName);
		return picToSave;
	}

}
