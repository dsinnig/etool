package com.concordia.etool.core;

import java.io.FileInputStream;
import java.io.IOException;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

public class PlayAudio {

	private MediaPlayer mPlayer;
	private int maxLength = 0;
	private boolean isPlaying;

	private static final PlayAudio instance = new PlayAudio();

	private PlayAudio() {
		
		mPlayer = new MediaPlayer();
		setPlaying(false);
		
		mPlayer.setOnCompletionListener(new OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer mp) {
				setPlaying(false);
			}
		});
	}

	public static PlayAudio getInstance() {
		return instance;
	}

	public void playAudio(String path){

		try 
		{
			FileInputStream fis = new FileInputStream(path);

			if(mPlayer!=null)
			{
				if(isPlaying())
				{
					playPauseToggle();
					resetAudio();
				}
				mPlayer.setDataSource(fis.getFD());
				mPlayer.prepare();
				setPlaying(true);
				mPlayer.start();
				setMaxLength(mPlayer.getDuration());
			}

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void playPauseToggle()
	{
		if(mPlayer!=null)
		{
			if(isPlaying())
			{
				mPlayer.pause();
				setPlaying(false);
			}
			else
			{
				setPlaying(true);
				mPlayer.start();
			}
		}
	}
	
	public void resetAudio() {
		mPlayer.reset();
	}

	public void stopAudio()
	{
		if(isPlaying)
			playPauseToggle();
		mPlayer.stop();
	}

	public void seekTo(int toPosition)
	{
		mPlayer.seekTo(toPosition);
	}

	public void releaseAudio()
	{
		if(mPlayer!=null)
			mPlayer.release();
	}


	/**
	 * @return the isPlaying
	 */
	public boolean isPlaying() {
		return isPlaying;
	}

	/**
	 * @param isPlaying the isPlaying to set
	 */
	public void setPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}

	/**
	 * @return the maxLength
	 */
	public int getMaxLength() {
		return maxLength;
	}

	/**
	 * @param maxLength the maxLength to set
	 */
	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public MediaPlayer getMediaPlayer() {
		return mPlayer;
	}

	public void setMediaPlayer(MediaPlayer mp) {
		this.mPlayer = mp;
	}

}
