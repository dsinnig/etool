package com.concordia.etool.core;

import java.io.IOException;

import android.media.MediaRecorder;
import android.util.Log;

public class RecordAudio {
	
	public static final int maxDuration = 5*60*1000;
	public MediaRecorder mRecorder;
	private boolean isRecording; 
	
	private static final RecordAudio instance = new RecordAudio();

	private RecordAudio() {
		mRecorder = new MediaRecorder();
		isRecording = false;
	}

	public static RecordAudio getInstance() {
		return instance;
	}
	
	public void record(String audioToPlay)
	{
		if(!isRecording) { 
			mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
			mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
			mRecorder.setOutputFile(audioToPlay);
			mRecorder.setMaxDuration(maxDuration);
			
			try {
				mRecorder.prepare();
				mRecorder.start();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				Log.e("Failed","prepare() failed");
				e.printStackTrace();
			}
			isRecording = true;
		}
	}

	public void stopRecording()
	{
		if(isRecording)
		{
			isRecording = false;
			mRecorder.stop();
		}
	}
	
	public void resetRecorder()
	{
		mRecorder.reset();
	}

	public boolean isRecording() {
		return isRecording;
	}

	public void setRecording(boolean isRecording) {
		this.isRecording = isRecording;
	}
}
