/**
 * 
 */
package com.concordia.etool.core;

/**
 * @author Ranjith
 *	Defines custom event to be triggered on prompt completion
 */
public interface OnPromptCompletion {
	public abstract void onComplete(PromptPlayer promptPlayer);
}
