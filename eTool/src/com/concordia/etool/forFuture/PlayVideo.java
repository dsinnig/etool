package com.concordia.etool.forFuture;

import android.widget.VideoView;

public class PlayVideo {
	
	
	boolean isPlaying;
	
	private static final PlayVideo instance = new PlayVideo();
	
	private PlayVideo() {
		
		isPlaying = false;
	}
	
	public static PlayVideo getInstance()
	{
		return instance;
	}
	
	public void playVideo(VideoView view,String path)
	{
		view.setVideoPath(path);
		view.start();
	}
	
	public void pauseVideo()
	{
		
	}

}
