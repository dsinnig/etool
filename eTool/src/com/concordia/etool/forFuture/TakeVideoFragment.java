package com.concordia.etool.forFuture;

import java.io.File;
import java.io.IOException;

import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.concordia.etool.R;
import com.concordia.etool.core.helpers.Preview;
import com.concordia.etool.core.helpers.StorageInterface;

public class TakeVideoFragment extends Fragment implements StorageInterface {

	boolean isRecording;
	MediaRecorder mMediaRecorder;


	private Camera mCamera;
	private Preview mPreview;

	private ImageButton recordButton;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		mCamera = getCameraInstance();
		mPreview = new Preview(getActivity());
		mPreview.setCamera(mCamera);
		
		return inflater.inflate(R.layout.video_layout, container, false);
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		//add preview to a display
				FrameLayout preview= (FrameLayout) getActivity().findViewById(R.id.video_preview);
				preview.addView(mPreview);

				isRecording = false;
				recordButton = (ImageButton) getActivity().findViewById(R.id.button1);
				recordButton.setImageResource(R.drawable.camera);

				recordButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (isRecording) {
							// stop recording and release camera
							mMediaRecorder.stop();  // stop the recording
							releaseMediaRecorder(); // release the MediaRecorder object
							mCamera.lock();         // take camera access back from MediaRecorder

							// inform the user that recording has stopped
							isRecording = false;
						} else {
							// initialize video camera
							if (prepareVideoRecorder()) {
								// Camera is available and unlocked, MediaRecorder is prepared,
								// now you can start recording
								mMediaRecorder.start();

								// inform the user that recording has started
								isRecording = true;
							} else {
								// prepare didn't work, release the camera
								releaseMediaRecorder();
								// inform user
							}
						}
					}
				});
				
	}

	
	@Override
	public void onPause() {
		super.onPause();
		
		releaseMediaRecorder();
		if(mCamera!=null) {
			
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
	}


	@Override
	public File saveMedia(String fileName) {
		String pathToSave = Environment.getExternalStorageDirectory().getAbsolutePath();
		pathToSave += eToolMainDir + eToolStoriesDir ;
		File picToSave = new File(pathToSave);
		if(!picToSave.exists())
			picToSave.mkdirs();
		picToSave = new File(pathToSave,fileName);
		return picToSave;
	}

	public static Camera getCameraInstance(){
		Camera c = null;
		try {
			c = Camera.open(); // attempt to get a Camera instance
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return c; // returns null if camera is unavailable
	}

	private boolean prepareVideoRecorder(){

		mMediaRecorder = new MediaRecorder();
		mMediaRecorder.reset();
		mMediaRecorder.release();

		mMediaRecorder = new MediaRecorder();
		// Step 1: Unlock and set camera to MediaRecorder
		mCamera.unlock();
		mMediaRecorder.setCamera(mCamera);

		// Step 2: Set sources
		mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
		mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

		// Step 3: Set a CamcorderProfile (requires API Level 8 or higher)
		mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));

		// Step 4: Set output file
		String pathToFile = saveMedia("etool.mp4").getAbsolutePath();
		mMediaRecorder.setOutputFile(pathToFile);

		// Step 5: Set the preview output
		mMediaRecorder.setPreviewDisplay(mPreview.getSurfaceHolder().getSurface());

		// Step 6: Prepare configured MediaRecorder
		try {
			mMediaRecorder.prepare();
		} catch (IllegalStateException e) {
			Log.d("Video Recorder", "IllegalStateException preparing MediaRecorder: " + e.getMessage());
			releaseMediaRecorder();
			return false;
		} catch (IOException e) {
			Log.d("Video Recorder", "IOException preparing MediaRecorder: " + e.getMessage());
			releaseMediaRecorder();
			return false;
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	private void releaseMediaRecorder() {
		if (mMediaRecorder != null) {
			mMediaRecorder.reset();   // clear recorder configuration
			mMediaRecorder.release(); // release the recorder object
			mMediaRecorder = null;
			mCamera.lock();           // lock camera for later use
		}
	}

}
