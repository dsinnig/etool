package com.concordia.etool.forFuture;

public class RecordVideo {
	
	private static final RecordVideo instance = new RecordVideo();

	private RecordVideo() {
	}

	public static RecordVideo getInstance() {
		return instance;
	}
}
