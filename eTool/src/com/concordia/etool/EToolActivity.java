package com.concordia.etool;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import com.concordia.etool.core.OnPromptCompletion;
import com.concordia.etool.core.PromptPlayer;
import com.concordia.etool.core.helpers.EToolApplication;
import com.concordia.etool.graph.Node;
import com.concordia.etool.parsers.ConfigParser;
import com.concordia.etool.parsers.DialogGraphParser;


public class EToolActivity extends Activity implements OnClickListener {

	ImageButton introButton;
	ImageButton storyLibButton;
	ImageButton tellStoryButton;

	Intent newActivityIntent;

	String[] prompts = {"mainScreen_1.wav"};
	String timeoutPrompts = "mainScreen_timeout.wav";
	long mStartTime = 0;

	PromptPlayer promptPlayer; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.etool_activity);
		
		promptPlayer = EToolApplication.getInstance(getApplicationContext()).getPromptPlayer();

		if(!EToolApplication.isReturning)
		{
			promptPlayer.playPrompts(prompts);
			EToolApplication.isReturning = true;
		}

		promptPlayer.setOnPromptCompleteListener(new OnPromptCompletion() {

			@Override
			public void onComplete(PromptPlayer promptPlayer) {

				mStartTime = SystemClock.currentThreadTimeMillis();
				timeoutHandler.postDelayed(timeoutRunner, 10000);
			}
		});

		introButton = (ImageButton) findViewById(R.id.introBtn);
		storyLibButton = (ImageButton) findViewById(R.id.libraryBtn);
		tellStoryButton = (ImageButton) findViewById(R.id.tellStoryBtn);

		DialogGraphParser parser = new DialogGraphParser(EToolApplication.mainDirectory +"/dialog_graph1.xml");
		EToolApplication.dialogGraph = parser.parse();

		ConfigParser cParser = new ConfigParser(EToolApplication.mainDirectory+"/etool.xml");
		cParser.parse();
		
		EToolApplication.initializeStories();
		
		introButton.setOnClickListener(this);
		storyLibButton.setOnClickListener(this);
		tellStoryButton.setOnClickListener(this);


	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent admin = new Intent(this,AdminChooseDirActivity.class);
		startActivity(admin);
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.options, menu);
		return true;
	}

	final Handler timeoutHandler = new Handler();
	Runnable timeoutRunner = new Runnable() {

		@Override
		public void run() {
			final long start = mStartTime;
			long millis = SystemClock.uptimeMillis() - start;
			int seconds = (int) (millis / 1000);
			int minutes = seconds / 60;
			seconds     = seconds % 60;

			if(promptPlayer.isPlaying() || !promptPlayer.isComplete())
				promptPlayer.stopPrompt();
			promptPlayer.playPrompt(timeoutPrompts);

			timeoutHandler.postAtTime(this,
					start + (((minutes * 60) + seconds + 1) * 10000));
		}
	};

	@Override
	protected void onPause() {
		super.onPause();

		if(promptPlayer.isPlaying() || ! promptPlayer.isComplete())
			promptPlayer.stopPrompt();

		timeoutHandler.removeCallbacks(timeoutRunner);
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		EToolApplication.isReturning = false;

	}

	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		
		case R.id.introBtn:
			
			//start introduction
			
			newActivityIntent = new Intent(EToolActivity.this, PlayStoryActivity.class);
			newActivityIntent.putExtra("storyName", EToolApplication.getDirectory("introDir"));
			startActivity(newActivityIntent);
			
			break;

		case R.id.libraryBtn:
			
			// start story library

			newActivityIntent = new Intent(EToolActivity.this, StoryLibraryActivity.class);
			startActivity(newActivityIntent);
			break;
		
		case R.id.tellStoryBtn:
			
			//start tell story

			EToolApplication.incrementNode(null);
			
			final Node tempNode = EToolApplication.dialogGraph.getFirst();
			
			if(tempNode != null)
			{
				switch (tempNode.getNodeType()) {

				case PLAY_AUDIO_RECORD:
					newActivityIntent= new Intent(EToolActivity.this,TellStoryActivity.class);
					break;

				case PLAY_AUDIO_TAKE_PICTURE:
					newActivityIntent = new Intent(EToolActivity.this, TellStoryTakePicture.class);
					break;

				case PLAY_AUDIO:
					newActivityIntent = new Intent(EToolActivity.this, TellStoryPlayAudio.class);
					
//					PlayAudio audioPlayer = EToolApplication.getInstance().getAudioPlayer();
//					audioPlayer.getMediaPlayer().setOnCompletionListener(new OnCompletionListener() {
//
//						@Override
//						public void onCompletion(MediaPlayer mp) {
//							EToolApplication.incrementNode(tempNode);
//							newActivityIntent = new Intent(EToolActivity.this,TellStoryActivity.class);
//							startActivity(newActivityIntent);
//						}
//					});
//					audioPlayer.playAudio(EToolApplication.getDirectory("questionsDir") + EToolApplication.currentNode.getAudio());
					
					break;
					
				case PLAY_AUDIO_KEYPAD_BRANCH:
					//
					break;
				case PLAY_VIDEO:
					//
					break;
				}
			}
			if(newActivityIntent!= null)				
				startActivity(newActivityIntent);
			
			break;
			
		default:
			break;
		}
	}




}