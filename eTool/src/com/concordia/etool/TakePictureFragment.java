/**
 * 
 */
package com.concordia.etool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.concordia.etool.core.OnPromptCompletion;
import com.concordia.etool.core.PlayAudio;
import com.concordia.etool.core.PromptPlayer;
import com.concordia.etool.core.helpers.EToolApplication;
import com.concordia.etool.core.helpers.Preview;
import com.concordia.etool.core.helpers.StorageInterface;
import com.concordia.etool.core.helpers.Story;
import com.concordia.etool.graph.Node;
import com.concordia.etool.ui.YesNoComponent;

/**
 * @author Ranjith
 *
 */
public class TakePictureFragment extends Fragment implements StorageInterface, OnClickListener {

	private static Camera mCamera;
	private Preview mPreview;
	PictureCallback mPictureCallBack;
	FrameLayout previewFrame;



	String[] timeoutPrompts = {"PAP_timeout.wav","PAP_stop_timeout.wav", "PAP_cancel_timeout.wav"};
	String [] navigationPrompts = {"PAP_cameraButton.wav","PAP_cancel.wav","PAP_cancel_stopsign.wav","PAP_cancel_trashcan.wav"};

	String currentTimeOutPrompt = timeoutPrompts[0];
	String currentNavigationPrompt = navigationPrompts[0];
	PromptPlayer promptPlayer;
	PlayAudio audioPlayer;
	
	

	Dialog picTaken;

	//default front camera
	public static int currentCameraID = CameraInfo.CAMERA_FACING_BACK;
	long mStartTime = 0;
	YesNoComponent yesNo;

	ImageButton cameraButton, yesButton, noButton;
	protected ImageButton disapproveDiscard, approveDiscard;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		super.onCreateView(inflater, container, savedInstanceState);

		mCamera = getCameraInstance(currentCameraID);
		mPreview = new Preview(getActivity());
		mPreview.setCamera(mCamera);

		//add take picture callback 
		mPictureCallBack = new PictureCallback() {

			@Override
			public void onPictureTaken(byte[] data, Camera camera) {

				File picToSave =  new File(getActivity().getDir(Integer.toString(Story.storyCount+1), Context.MODE_PRIVATE).getAbsolutePath()+File.separator+EToolApplication.currentNode.getId()+".jpg");
				//			data = rotatePicImg(data);
				try {
					
					Camera.CameraInfo info = new Camera.CameraInfo();
					Camera.getCameraInfo(currentCameraID, info);
					ExifInterface exif = new ExifInterface(picToSave.getAbsolutePath());
					
					if(info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
						exif.setAttribute(ExifInterface.TAG_ORIENTATION, Integer.toString(ExifInterface.ORIENTATION_NORMAL));
					else
						exif.setAttribute(ExifInterface.TAG_ORIENTATION, Integer.toString(ExifInterface.ORIENTATION_ROTATE_180));

					FileOutputStream fos = new FileOutputStream(picToSave);
					fos.write(data);
					fos.close();
					exif.saveAttributes();
					
					mCamera.stopPreview();
					mCamera.release();
					mCamera = null;
					
					ImageView img = new ImageView(getActivity());
					LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) previewFrame.getLayoutParams();
					lp.gravity = Gravity.CENTER;
					lp.width = 300;
					lp.height = 300;
					img.setLayoutParams(lp);
					Bitmap tempImgToRotate = BitmapFactory.decodeFile(picToSave.getAbsolutePath());
					tempImgToRotate = EToolApplication.rotateBitmap(tempImgToRotate, EToolApplication.findImageRotation(picToSave.getAbsolutePath()));
					img.setImageBitmap(tempImgToRotate);
					
					previewFrame.removeAllViews();
					previewFrame.addView(img);	
					yesNo.setVisibility(View.VISIBLE);
					cameraButton.setVisibility(View.INVISIBLE);

					currentTimeOutPrompt = timeoutPrompts[0];
					currentNavigationPrompt = navigationPrompts[0];
					promptPlayer.playPrompt(currentNavigationPrompt);
					
					
				} catch (FileNotFoundException e) {
					Log.d("Picture Callback error", "File not found: " + e.getMessage());
				} catch (IOException e) {
					Log.d("Picture Callback error", "Error accessing file: " + e.getMessage());
				}
			}
		};

		return inflater.inflate(R.layout.take_picture, container, true);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		//add preview to a display
		
		previewFrame = (FrameLayout) getActivity().findViewById(R.id.camera_preview);
		cameraButton = (ImageButton) getActivity().findViewById(R.id.takePicBtn);
		previewFrame = (FrameLayout) getActivity().findViewById(R.id.camera_preview);
		yesNo = (YesNoComponent) getActivity().findViewById(R.id.confirmSnapshot);
		yesButton = (ImageButton) getActivity().findViewById(R.id.imageApprove);
		noButton = (ImageButton) getActivity().findViewById(R.id.imageDecline);
		
		
		//set up dialog
		picTaken = new Dialog(getActivity());
		picTaken.setContentView(R.layout.record_confirm_dialog);
		picTaken.setCancelable(true);
		picTaken.setOnShowListener(new OnShowListener() {


			@Override
			public void onShow(DialogInterface dialog) {
				//dialog action buttons
				approveDiscard = (ImageButton) picTaken.findViewById(R.id.discardApprove);
				disapproveDiscard = (ImageButton) picTaken.findViewById(R.id.discardDecline);
				setListeners();
			}
		});

		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) previewFrame.getLayoutParams();
		lp.gravity = Gravity.CENTER_HORIZONTAL;
		previewFrame.setLayoutParams(lp);
		previewFrame.requestLayout();
		previewFrame.addView(mPreview);
		cameraButton.setOnClickListener(this);
		previewFrame.setOnClickListener(this);
		yesButton.setOnClickListener(this);
		noButton.setOnClickListener(this);

		audioPlayer = EToolApplication.getInstance(getActivity()).getAudioPlayer();
		audioPlayer.playAudio(EToolApplication.getDirectory("questionsDir")+ EToolApplication.currentNode.getAudio());
		audioPlayer.getMediaPlayer().setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {

				mStartTime = SystemClock.currentThreadTimeMillis();
				timeoutHandler.postDelayed(timeoutRunner, 10000);
			}
		});

		promptPlayer = EToolApplication.getInstance(getActivity()).getPromptPlayer();
		promptPlayer.setOnPromptCompleteListener(new OnPromptCompletion() {
			@Override
			public void onComplete(PromptPlayer promptPlayer) {
				
				mStartTime = SystemClock.currentThreadTimeMillis();
				timeoutHandler.postDelayed(timeoutRunner, 10000);
			}
		});
	}

	protected void setListeners() {
		disapproveDiscard.setOnClickListener(this);
		approveDiscard.setOnClickListener(this);	
	}

	@Override
	public void onPause() {
		super.onPause();

		if(mCamera!=null)
		{
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
		}
		
		promptPlayer.stopPrompt();
		audioPlayer.resetAudio();
		timeoutHandler.removeCallbacks(timeoutRunner);
	}

	@Override
	public void onResume() {
		super.onResume();
		//instantiate camera and initialize preview 
		if(mCamera == null)
			mCamera = getCameraInstance(currentCameraID);

		if(mPreview == null)
			mPreview = new Preview(getActivity()); 
		mPreview.setCamera(mCamera);	
	}

	public static Camera getCameraInstance(int cameraId){
		Camera c = null;
		try {
			if(mCamera==null)
				c = Camera.open(cameraId); // attempt to get a Camera instance
			else c = mCamera;

		}
		catch (Exception e){
			e.printStackTrace();
		}
		return c; // returns null if camera is unavailable
	}

	@Override
	public File saveMedia(String fileName) {
		String pathToSave = eToolMainDir + eToolStoriesDir ;
		File picToSave = new File(pathToSave);
		if(!picToSave.exists())
			picToSave.mkdirs();
		picToSave = new File(pathToSave,fileName);
		return picToSave;
	}

	@Override
	public void onClick(View v) {	

		timeoutHandler.removeCallbacks(timeoutRunner);

		switch (v.getId()) {

		case R.id.takePicBtn:
			try {
				
				mCamera = getCameraInstance(currentCameraID);
				mCamera.takePicture(null, null, mPictureCallBack);

			} catch (Exception e)
			{
				e.printStackTrace();
			}
			break;

		case R.id.camera_preview:
			if(currentCameraID == CameraInfo.CAMERA_FACING_FRONT)
				currentCameraID = CameraInfo.CAMERA_FACING_BACK;
			else
				currentCameraID = CameraInfo.CAMERA_FACING_FRONT;
			mCamera.stopPreview();
			mCamera.release();
			mCamera = null;
			mCamera = getCameraInstance(currentCameraID);
			Preview.setCameraDisplayOrientation(getActivity(), currentCameraID, mCamera);
			mCamera.startPreview();
			mPreview.switchCamera(mCamera);
			break;

		case R.id.questionComponent:
			
			if(audioPlayer.isPlaying())
				audioPlayer.resetAudio();
			audioPlayer.playAudio(EToolApplication.getDirectory("questionsDir") + EToolApplication.currentNode.getAudio());

			break;

		case R.id.imageApprove:
			//move to new new questions
			Intent newQuestionIntent = null;

			Node tempNode = EToolApplication.dialogGraph.getNextNode(EToolApplication.currentNode);

			if(tempNode != null)
			{
				switch (tempNode.getNodeType()) {

				case PLAY_AUDIO_RECORD:
					newQuestionIntent= new Intent(getActivity(),TellStoryActivity.class);
					break;

				case PLAY_AUDIO_TAKE_PICTURE:
					newQuestionIntent = new Intent(getActivity(), TellStoryTakePicture.class);
					break;

				case PLAY_AUDIO:
					newQuestionIntent = new Intent(getActivity(), TellStoryPlayAudio.class);
					break;
				case PLAY_AUDIO_KEYPAD_BRANCH:
					//
					break;
				case PLAY_VIDEO:
					//
					break;
				}
				EToolApplication.incrementNode(EToolApplication.currentNode);
			}
			
			else
				newQuestionIntent = new Intent(getActivity(), TellStoryEndActivity.class);

			startActivity(newQuestionIntent);
			
			break;

		case R.id.imageDecline:
			//show dialog
			
			picTaken.show();
			currentNavigationPrompt = navigationPrompts[1];
			promptPlayer.playPrompt(currentNavigationPrompt);
			currentTimeOutPrompt = timeoutPrompts[2];
			break;
			
		case R.id.discardApprove:
			
			currentNavigationPrompt = navigationPrompts[3];
			promptPlayer.playPrompt(currentNavigationPrompt);
			currentTimeOutPrompt = timeoutPrompts[0];
			
			mCamera = getCameraInstance(Camera.CameraInfo.CAMERA_FACING_FRONT);
			mPreview = new Preview(getActivity());
			mPreview.setCamera(mCamera);
			previewFrame.removeAllViews();
			previewFrame.addView(mPreview);
			
			picTaken.dismiss();
			yesNo.setVisibility(View.GONE);
			cameraButton.setVisibility(View.VISIBLE);
			
			break;
		case R.id.discardDecline:
			
			currentNavigationPrompt = navigationPrompts[2];
			promptPlayer.playPrompt(currentNavigationPrompt);
			currentTimeOutPrompt = timeoutPrompts[0];
			picTaken.dismiss();
			
			break;

		default:
			break;
		}
	}

	final Handler timeoutHandler = new Handler();
	Runnable timeoutRunner = new Runnable() {

		@Override
		public void run() {
			final long start = mStartTime;
			long millis = SystemClock.uptimeMillis() - start;
			int seconds = (int) (millis / 1000);
			int minutes = seconds / 60;
			seconds     = seconds % 60;

			promptPlayer.playPrompt(currentTimeOutPrompt);

			timeoutHandler.postAtTime(this,
					start + (((minutes * 60) + seconds + 1) * 10000));
		}
	};

}
