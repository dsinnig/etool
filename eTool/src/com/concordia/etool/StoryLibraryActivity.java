package com.concordia.etool;

import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.concordia.etool.core.OnPromptCompletion;
import com.concordia.etool.core.PromptPlayer;
import com.concordia.etool.core.helpers.EToolApplication;
import com.concordia.etool.core.helpers.LazyImageAdapter;

public class StoryLibraryActivity extends Activity {
	

	GridView storyLibrary;
	PromptPlayer promptPlayer;
	
	String[] prompts = {"storylibrary_1.wav"};
	String timeoutPrompts = "storyLibrary_timeout.wav";
	
	long mStartTime = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.story_library);
		
		storyLibrary = (GridView) findViewById(R.id.gridview);
		
	}
	
	public void startPormpts()
	{
		promptPlayer = EToolApplication.getInstance(getApplicationContext()).getPromptPlayer();
		promptPlayer.playPrompts(prompts);
		
		promptPlayer.setOnPromptCompleteListener(new OnPromptCompletion() {
			
			@Override
			public void onComplete(PromptPlayer promptPlayer) {
				
				mStartTime = SystemClock.currentThreadTimeMillis();
				timeoutHandler.postDelayed(timeoutRunner, 5000);
			}
		});
	}
	
	final Handler timeoutHandler = new Handler();
	Runnable timeoutRunner = new Runnable() {
		
		@Override
		public void run() {
			final long start = mStartTime;
		       long millis = SystemClock.uptimeMillis() - start;
		       int seconds = (int) (millis / 1000);
		       int minutes = seconds / 60;
		       seconds     = seconds % 60;
		       
		       promptPlayer.playPrompt(timeoutPrompts);
		       
		       timeoutHandler.postAtTime(this,
		               start + (((minutes * 60) + seconds + 1) * 4000));
		}
	};
	
	@Override
	protected void onPause() {
		super.onPause();
		
		if(promptPlayer.isPlaying())
			promptPlayer.stopPrompt();
		
		timeoutHandler.removeCallbacks(timeoutRunner);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		storyLibrary.setAdapter(new LazyImageAdapter(getApplicationContext(), EToolApplication.getDirectory("libDir")));
		
		storyLibrary.setOnItemClickListener(new OnItemClickListener() {
			
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
				
				Intent playStoryIntent = new Intent(StoryLibraryActivity.this , PlayStoryActivity.class);
				String temp = EToolApplication.getDirectory("libDir") + LazyImageAdapter.getDirForImg(position) + File.separator;
				playStoryIntent.putExtra ("storyName", temp);
				startActivity(playStoryIntent);
			}
		});
		
		startPormpts();
	}
	
	
}
