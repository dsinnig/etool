package com.concordia.etool;

import java.io.File;
import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ProgressBar;
import android.widget.ViewSwitcher;

import com.concordia.etool.core.PlayAudio;
import com.concordia.etool.core.helpers.EToolApplication;
import com.concordia.etool.core.helpers.ImageLoader;
import com.concordia.etool.core.helpers.ImageLoader.ScalingLogic;
import com.concordia.etool.core.helpers.PlayerDataType;
import com.concordia.etool.core.helpers.StoryPlayerData;

public class PlayStoryActivity extends Activity implements  OnClickListener{

	ImageButton playBtn, pauseBtn, skipBackBtn, skipFwdBtn;
	String currentStory, currentPic, currentVideo, currentMedia,currentScript;
	ArrayList<String> stories;
	PlayAudio pa;

	ViewSwitcher visualSwitcher;
	StoryPlayerData playerHelper;
	ImageView imgView;
	Bundle b;
	private boolean isComplete = false;
	private boolean isPlaying = false;
	
	private int displaySize;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.play_story);

		b = getIntent().getExtras();
		if(b!=null)
		{
			setUpMediaToPlay(b);
		}

		setUpViews();

		pa = EToolApplication.getInstance(getApplicationContext()).getAudioPlayer();
		pa.getMediaPlayer().setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				if( playerHelper.getData().indexOf(currentMedia) < playerHelper.getData().size())
					handleNextMedia();
			}
		});
		pa.playAudio(currentScript + File.separator + currentStory);
		isPlaying = true;
	}

	private void setUpMediaToPlay(Bundle b)
	{
		//get current directory 
		currentScript = b.get("storyName").toString();

		//build media content to play
		playerHelper = new StoryPlayerData(currentScript);
		currentStory = currentMedia = playerHelper.getNextMedia(0,PlayerDataType.audio_type);
		currentPic = playerHelper.getNextMedia(0, PlayerDataType.picture_type);
		//currentVideo = playerHelper.getNextMedia(0, PlayerDataType.video_type);


		//currently only list of stories and 0/1 pic & video supported
		stories = playerHelper.getAllMediaOfType(PlayerDataType.audio_type);

	}

	private void resetImages()
	{
		if(imgView == null)
			imgView = new ImageView(this);

		if(visualSwitcher.getChildCount()>0)
			visualSwitcher.removeAllViews();

		visualSwitcher.addView(imgView);
		if(currentPic!=null)
		{
			AsynImgLoader imgLoader = new AsynImgLoader();
			imgLoader.execute(currentScript + currentPic);
		}
		else
		{
			imgView.setImageResource(R.drawable.default_user);
			visualSwitcher.setDisplayedChild(1);
		}
		
		if(EToolApplication.Screenheight > EToolApplication.Screenwidth) {
			displaySize = (int) Math.round( EToolApplication.Screenwidth); 
		}
		else {
			displaySize = (int) Math.round(EToolApplication.Screenheight); 
		}
		imgView.setScaleType(ScaleType.CENTER_INSIDE);
		imgView.setLayoutParams(new ViewSwitcher.LayoutParams(displaySize,displaySize));
	}

	private void setUpViews()
	{
		playBtn = (ImageButton) findViewById(R.id.playStoryPlay);
		pauseBtn = (ImageButton) findViewById(R.id.pauseStoryPlay);
		skipBackBtn = (ImageButton) findViewById(R.id.skipBack);
		skipFwdBtn = (ImageButton) findViewById(R.id.skipForward);
		visualSwitcher = (ViewSwitcher) findViewById(R.id.visualSwitcher);

		playBtn.setOnClickListener(this);
		pauseBtn.setOnClickListener(this);
		skipBackBtn.setOnClickListener(this);
		skipFwdBtn.setOnClickListener(this);

		ProgressBar progressBar = new ProgressBar(this);
		//change to relative
		progressBar.setLayoutParams(new ViewSwitcher.LayoutParams(150,150));
		visualSwitcher.addView(progressBar);
		visualSwitcher.setDisplayedChild(0);

		resetImages();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.playStoryPlay:
			if(isComplete)
			{
				if(!isPlaying)
				{
					setUpMediaToPlay(b);
					resetImages();
					isComplete = false;
					if(pa.isPlaying())
						pa.resetAudio();
					pa.playAudio(currentScript + currentStory);
					isPlaying = true;
				}
			}
			else
			{
				if(!isPlaying)
				{
					pa.playPauseToggle();
					isPlaying = true;
				}
				currentMedia = currentStory;
			}
			break;

		case R.id.pauseStoryPlay:
			if(isPlaying)
			{
				pa.playPauseToggle();
				isPlaying = false;
			}
			break;

		case R.id.skipBack:
			//skip to previous story

			break;
		case R.id.skipForward:
			//skip to previous story
			if(playerHelper.getData().indexOf(currentMedia) < playerHelper.getData().size())
				handleNextMedia();
			break;
		default:
			break;
		}

	}

	private void handleNextMedia()
	{
		int currentIndex = playerHelper.getIndex(currentMedia);
		if(currentIndex<playerHelper.getData().size()-1 && (!isComplete || isPlaying))
		{
			currentMedia = playerHelper.getNextMedia(currentIndex);

			switch (playerHelper.getNextMediaType(currentIndex)) {

			case PlayerDataType.picture_type:
				if(currentMedia!=null)
				{
					if (!currentPic.equalsIgnoreCase(currentMedia)) 
					{
						currentPic = currentMedia;
						AsynImgLoader loader = new AsynImgLoader();
						loader.execute(currentScript + currentPic);
					}
					handleNextMedia();
				}
				break;

			case PlayerDataType.audio_type:

				if(currentMedia!=null)
				{
					if(currentMedia.equalsIgnoreCase(currentStory) && stories.indexOf(currentStory)<stories.size())
						currentStory = stories.get(stories.indexOf(currentStory)+1);
					currentStory = currentMedia;

					pa.playAudio(currentScript + currentStory);
				}

				break;
			}
		}

		else
		{
			isComplete = true;
			isPlaying = false;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		if(pa.isPlaying())
			pa.resetAudio();

		isPlaying = false;
		isComplete = true;
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	class AsynImgLoader extends AsyncTask<String, Void, Bitmap>
	{

		@Override
		protected Bitmap doInBackground(String... params) {
			Bitmap rotateImg = ImageLoader.decodeFile(params[0], displaySize, displaySize, ScalingLogic.FIT);
			rotateImg = ImageLoader.createScaledBitmap(rotateImg, displaySize, displaySize, ScalingLogic.FIT);
			rotateImg = EToolApplication.rotateBitmap(rotateImg, EToolApplication.findImageRotation(currentScript + currentPic));
			return rotateImg;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			imgView.setImageBitmap(result);
			visualSwitcher.setDisplayedChild(1);
		}

	}

}
