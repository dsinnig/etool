package com.concordia.etool;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.concordia.etool.core.helpers.EToolApplication;
import com.concordia.etool.parsers.ConfigParser;

public class AdminChooseDirActivity extends Activity implements OnItemSelectedListener{

	String[] introDirs, libDirs, navDirs, questionsDir;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.admin_choose_dir);
		
		getDirs();
		
		Spinner sp = (Spinner) findViewById(R.id.introSpinner);
		ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, introDirs);
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp.setAdapter(aa);
//		sp.setSelection(getCurrentDir(EToolApplication.currentIntroductionPromptsDirectory));
		sp.setOnItemSelectedListener(this);
		
		Spinner sp1 = (Spinner) findViewById(R.id.questionSpinner);
		ArrayAdapter<String> aa1 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, questionsDir);
		aa1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp1.setAdapter(aa1);
		sp1.setSelection(getCurrentDir(EToolApplication.currentQuestionsPromptsDirectory));
		sp1.setOnItemSelectedListener(this);
		
		Spinner sp2 = (Spinner) findViewById(R.id.navSpinner);
		ArrayAdapter<String>  aa2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, navDirs);
		aa2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp2.setAdapter(aa2);
		sp2.setSelection(getCurrentDir(EToolApplication.currentNavigationPromptsDirectory));
		sp2.setOnItemSelectedListener(this);
		
		Spinner sp3 = (Spinner) findViewById(R.id.libSpinner);
		ArrayAdapter<String> aa3 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, libDirs);
		aa3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sp3.setAdapter(aa3);
		sp3.setSelection(getCurrentDir(EToolApplication.currentLibDirectory));
		sp3.setOnItemSelectedListener(this);
	}
	
	private int getCurrentDir(String dirToSelect) {
		int i = 0;
		int toReturn = 0;
		dirToSelect = dirToSelect.substring(dirToSelect.indexOf(File.separator)+1, dirToSelect.lastIndexOf(File.separator));
		while(i<questionsDir.length)
		{
			if(questionsDir[i].equalsIgnoreCase(dirToSelect))
				toReturn = i;
			i++;
		}
		return toReturn;
	}

	public void getDirs()
	{
		File tempFile;
		
		tempFile = new File(EToolApplication.getDirectory("mainIntroDir"));
		if(tempFile.exists())
			introDirs = tempFile.list();
		
		tempFile = new File(EToolApplication.getDirectory("mainNavDir"));
		if(tempFile.exists())
			navDirs = tempFile.list();
		
		tempFile = new File(EToolApplication.getDirectory("mainLibDir"));
		if(tempFile.exists())
			libDirs = tempFile.list();
		
		tempFile = new File(EToolApplication.getDirectory("mainQuestionsDir"));
		if(tempFile.exists())
			questionsDir = tempFile.list();
	}

	

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		String selectedDir = File.separator+ (String) arg0.getItemAtPosition(arg2) + File.separator;
		switch (arg0.getId()) {
		case R.id.libSpinner:
			EToolApplication.currentLibDirectory = selectedDir;
			break;
		case R.id.navSpinner:
			EToolApplication.currentNavigationPromptsDirectory = selectedDir;
			break;
		case R.id.questionSpinner:
			EToolApplication.currentQuestionsPromptsDirectory = selectedDir;
			break;
		case R.id.introSpinner:
			EToolApplication.currentIntroductionPromptsDirectory = selectedDir;
			break;
		default:
			break;
		}
	}
	

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(EToolApplication.mainDirectory + "/etool.xml"));
			writer.write((new ConfigParser(null)).toStringXMl());
			writer.flush();
			
			EToolApplication.initializeStories();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
