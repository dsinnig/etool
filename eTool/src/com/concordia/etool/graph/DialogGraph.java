package com.concordia.etool.graph;

import java.util.LinkedList;

import com.concordia.etool.parsers.NodeType;

public class DialogGraph extends LinkedList<Node>{

	private static final long serialVersionUID = 1L;

	private Node initialNode;

	public static int size = 0;

	public DialogGraph(Node initialNode) {
		super();

		this.incrementSize();
	}

	private void incrementSize()
	{
		size ++;
	}

	private void decrementSize()
	{
		size --;
	}

	public Node getPicNode()
	{
		Node tempNode = this.initialNode;
		while(tempNode!= null)
		{
			if(tempNode.getNodeType() == NodeType.PLAY_AUDIO_TAKE_PICTURE)
				return tempNode;

			tempNode = tempNode.getSuccessor(); 
		}
		return tempNode;
	}

	@Override
	public boolean add(Node object) {
		boolean result = false;

		Node tempNode = initialNode;
		do
		{
			if(tempNode==null)
			{
				this.initialNode = tempNode = object;
				break;
			}
			if(tempNode.getSuccessor()==null)
			{
				tempNode.setSuccessor(object);
				this.incrementSize();
				result = true;
				break;
			}
			else
			{
				tempNode= tempNode.getSuccessor();
			}
		} while(tempNode!=null);


		return result; 
	}

	@Override
	public Node remove() {

		Node tempNode = initialNode;
		Node toReturn = null;
		while (tempNode!=null) {
			if(tempNode.getSuccessor().getSuccessor()==null)
			{
				toReturn = tempNode.getSuccessor();
				this.decrementSize();
				tempNode.setSuccessor(null);
				break;
			}

			tempNode = tempNode.getSuccessor();
		}

		return toReturn;
	}

	@Override
	public int size() {
		return DialogGraph.size;
	}

	public Node getNextNode(Node n)
	{
		Node tempNode = this.initialNode; 
		while(tempNode!=null){
			if( n != null && tempNode.equals(n))
				return tempNode.getSuccessor();
			else
				tempNode = tempNode.getSuccessor();
		}
		return null;
	}

	public Node getPrevNode(Node n)
	{
		Node tempNode = this.initialNode;
		if(n == tempNode)
			return null;
		while(tempNode!=null)
		{
			if(n!= null && tempNode.getSuccessor().equals(n))
				return tempNode;
			else
				tempNode = tempNode.getSuccessor();
		}
		return null;
	}

	@Override
	public Node get(int id) {
		Node tempNode = this.initialNode;
		while(tempNode!=null)
		{
			if(tempNode.getId() == id)
				break;
			tempNode = tempNode.getSuccessor();
		}

		return tempNode;
	}

	@Override
	public Node getFirst() {
		return this.initialNode;
	}

	@Override
	public Node getLast() {
		Node tempNode = this.initialNode;
		while(tempNode!=null)
		{
			if(tempNode.getSuccessor() == null)
				break;
			tempNode = tempNode.getSuccessor();
		}
		return tempNode;
	}

	@Override
	public int indexOf(Object object) {
		Node tempNode = this.initialNode;
		int id = 0;
		while(tempNode != null)
		{
			if(tempNode.equals(object))
			{
				id = tempNode.getId();
				break;
			}
			tempNode = tempNode.getSuccessor();
		}

		return id;
	}

	public static String toXmlString()
	{
		return null;
	}
}
