package com.concordia.etool.graph;

import java.io.Serializable;

import com.concordia.etool.parsers.NodeType;
import com.concordia.etool.parsers.NodeTypeXML;

public class Node implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Node successor;
	private NodeType nodeType;
	private String image;
	private String audio;
	private String video;
	private int id;

	public Node(Node nextNode, String type )
	{
		this.successor = nextNode;
		setNodeType(type);

	}

	/**
	 * @return the nodeType
	 */
	public NodeType getNodeType() {
		return nodeType;
	}

	/**
	 * @param nodeType the nodeType to set
	 */
	public void setNodeType(String nodeType) {

		if(nodeType.equalsIgnoreCase(NodeTypeXML.PLAY_AUDIO_RECORD))
			this.nodeType = NodeType.PLAY_AUDIO_RECORD;
		else if(nodeType.equalsIgnoreCase(NodeTypeXML.PLAY_AUDIO_TAKE_PICTURE))
			this.nodeType = NodeType.PLAY_AUDIO_TAKE_PICTURE;
		else if(nodeType.equalsIgnoreCase(NodeTypeXML.PLAY_AUDIO_KEYPAD_BRANCH))
			this.nodeType = NodeType.PLAY_AUDIO_KEYPAD_BRANCH;
		else if(nodeType.equalsIgnoreCase(NodeTypeXML.PLAY_AUDIO))
			this.nodeType = NodeType.PLAY_AUDIO;
		else if(nodeType.equalsIgnoreCase(NodeTypeXML.PLAY_VIDEO))
			this.nodeType = NodeType.PLAY_VIDEO;
	}



	@Override
	public boolean equals(Object o) {

		return this.id== ((Node)o).getId();
	}

	/**
	 * @return the successor
	 */
	public Node getSuccessor() {
		return successor;
	}

	/**
	 * @param successor the successor to set
	 */
	public void setSuccessor(Node successor) {
		this.successor = successor;
	}

	/**
	 * @return the image
	 */
	public String getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(String image) {
		this.image = image;
	}

	/**
	 * @return the audio
	 */
	public String getAudio() {
		return audio;
	}

	/**
	 * @param audio the audio to set
	 */
	public void setAudio(String audio) {
		this.audio = audio;
	}

	/**
	 * @return the video
	 */
	public String getVideo() {
		return video;
	}

	/**
	 * @param video the video to set
	 */
	public void setVideo(String video) {
		this.video = video;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

}