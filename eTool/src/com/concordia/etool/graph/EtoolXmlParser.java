package com.concordia.etool.graph;

public abstract class EtoolXmlParser {
	
	static final String ROOT = "dialog";
	static final String NODE = "node";
	static final String DESCRIPTION = "description";
	static final String IMAGE_FILE = "imageFileName";
	static final String AUDIO_FILE = "audioFileName";
	static final String VIDEO_FILE = "videoFileName";
	static final String SUCCESSORS = "successors";
	static final String SUCCESSOR = "suscessor";
	static final String NODE_TYPE = "type";
	static final String NODE_ID = "id";
	
	final String xmlFileName;

	public EtoolXmlParser(String xmlFileName) {
		super();
		this.xmlFileName = xmlFileName;
	}
	
}
