package com.concordia.etool;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.concordia.etool.core.PlayAudio;
import com.concordia.etool.core.helpers.EToolApplication;
import com.concordia.etool.core.helpers.Story;

public class TellStoryEndActivity extends Activity {

	FrameLayout storyPreviewFrame;
	ImageButton yesBtn, noBtn;

	String timeoutPrompt = "TS_end_timeout.wav";
	String[] navigationPrompts = {"TS_end.wav","TS_end_checkmark.wav", "TS_end_cross.wav"};
	String currentNavigationPrompt = navigationPrompts[0];
	PlayAudio promptPlayer;

	ViewSwitcher storyPreview;
	long mStartTime;
	File tempDir;
	String pathToSave;


	boolean isComplete = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.end_tell_story);
		tempDir= getDir(Integer.toString(Story.storyCount+1), Context.MODE_PRIVATE);
		pathToSave = EToolApplication.getDirectory("libDir") + Integer.toString(Story.storyCount+1);

		promptPlayer = EToolApplication.getInstance(getApplicationContext()).getAudioPlayer();

		storyPreviewFrame = (FrameLayout) findViewById(R.id.currentStoryDisplay);
		storyPreview = (ViewSwitcher) storyPreviewFrame.findViewById(R.id.imageLibItem);
		
		storyPreview.setLayoutParams(new FrameLayout.LayoutParams(500, 500));
		ImageView imgView = new ImageButton(this);
		imgView.setLayoutParams(new ViewSwitcher.LayoutParams(500,500));
		imgView.setScaleType(ScaleType.CENTER_CROP);
		String picPath = tempDir + File.separator + EToolApplication.dialogGraph.getPicNode().getId() + ".jpg";
		imgView.setImageBitmap(EToolApplication.rotateBitmap(BitmapFactory.decodeFile( picPath ), EToolApplication.findImageRotation(picPath)));
		storyPreview.addView(imgView);

		storyPreview.setDisplayedChild(0);

		TextView textStoryNumber = (TextView) storyPreviewFrame.findViewById(R.id.textLibItem);
		textStoryNumber.setTextSize(35);
		textStoryNumber.setPadding(5, 5, 5, 5);
		textStoryNumber.setText(Integer.toString(Story.storyCount));
		promptPlayer.playAudio(EToolApplication.getDirectory("navDir")+currentNavigationPrompt);
		promptPlayer.getMediaPlayer().setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				timeoutHandler.removeCallbacks(timeoutRunner);
				if(isComplete)
				{
					Intent mainMenu = new Intent(TellStoryEndActivity.this,EToolActivity.class);
					mainMenu.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(mainMenu);
				}
				else
				{
					mStartTime = SystemClock.currentThreadTimeMillis();
					timeoutHandler.postDelayed(timeoutRunner, 10000);
				}

			}
		});

		TextView txtView = ((TextView) storyPreviewFrame.findViewById(R.id.textLibItem));
		txtView.setText(Integer.toString(Story.storyCount+1));

		yesBtn = (ImageButton) findViewById(R.id.imageApprove);
		noBtn = (ImageButton) findViewById(R.id.imageDecline);

		yesBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Story.incrementStories();

				for(File f: tempDir.listFiles())
				{
					String tempPath = pathToSave + File.separator + f.getName();
					File tempStoryDir = new File(pathToSave);
					if(!tempStoryDir.exists())
						tempStoryDir.mkdirs();
					try {
						copyFile(f, new File(tempPath));
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				tempDir.delete();
				currentNavigationPrompt = navigationPrompts[1];
				promptPlayer.playAudio(EToolApplication.getDirectory("navDir") + currentNavigationPrompt);
				isComplete = true;
				
				timeoutHandler.removeCallbacks(timeoutRunner);
			}
		});

		noBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				currentNavigationPrompt = navigationPrompts[2];
				promptPlayer.playAudio(EToolApplication.getDirectory("navDir")+currentNavigationPrompt);
				isComplete = true;
				
				timeoutHandler.removeCallbacks(timeoutRunner);
			}
		});

	}

	final Handler timeoutHandler = new Handler();
	Runnable timeoutRunner = new Runnable() {

		@Override
		public void run() {
			final long start = mStartTime;
			long millis = SystemClock.uptimeMillis() - start;
			int seconds = (int) (millis / 1000);
			int minutes = seconds / 60;
			seconds     = seconds % 60;

			promptPlayer.playAudio(EToolApplication.getDirectory("navDir")+ timeoutPrompt);

			timeoutHandler.postAtTime(this,
					start + (((minutes * 60) + seconds + 1) * 10000));
		}
	};

	public static void copyFile(File sourceFile, File destFile) throws IOException {
		if(!destFile.exists()) {
			destFile.createNewFile();
		}

		FileChannel source = null;
		FileChannel destination = null;

		try {
			source = new FileInputStream(sourceFile).getChannel();
			destination = new FileOutputStream(destFile).getChannel();
			destination.transferFrom(source, 0, source.size());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if(source != null) {
				source.close();
			}
			if(destination != null) {
				destination.close();
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		timeoutHandler.removeCallbacks(timeoutRunner);
	}



}
