package com.concordia.etool.ui;

import com.concordia.etool.R;

import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

public class MediaControl extends LinearLayout {

	public MediaControl(Context context) {
		super(context);
		
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
			setOrientation(HORIZONTAL);
		else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
			setOrientation(VERTICAL);
		
		setGravity(Gravity.CENTER_HORIZONTAL);
		
		LayoutInflater.from(context).inflate(R.layout.media_controls, this);
	}

	public MediaControl(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
			setOrientation(HORIZONTAL);
		else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
			setOrientation(VERTICAL);
		
		setGravity(Gravity.CENTER_HORIZONTAL);
		
		LayoutInflater.from(context).inflate(R.layout.media_controls, this);
	}

	@Override
	protected void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			setOrientation(VERTICAL);
		
		else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
			setOrientation(HORIZONTAL);
	}

}
