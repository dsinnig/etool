package com.concordia.etool.ui;

import com.concordia.etool.R;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

public class LibraryItem extends FrameLayout {

	public LibraryItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		LayoutInflater.from(context).inflate(R.layout.library_item, this);
	}

}
