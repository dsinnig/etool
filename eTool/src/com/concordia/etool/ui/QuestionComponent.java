package com.concordia.etool.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.concordia.etool.R;

public class QuestionComponent extends FrameLayout {
	
	

	public QuestionComponent(Context context,
			AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		LayoutInflater.from(context).inflate(R.layout.question_component, this);
	}

	public QuestionComponent(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		LayoutInflater.from(context).inflate(R.layout.question_component, this);
	}

}
