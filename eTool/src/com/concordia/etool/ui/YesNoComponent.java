package com.concordia.etool.ui;

import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.concordia.etool.R;

public class YesNoComponent extends LinearLayout {

	public YesNoComponent(Context context) {
		super(context);
		
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
			setOrientation(HORIZONTAL);
		else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
			setOrientation(VERTICAL);
		
		setGravity(Gravity.CENTER_HORIZONTAL);
		
		LayoutInflater.from(context).inflate(R.layout.yes_no_component, this);
	
	}

	public YesNoComponent(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
			setOrientation(HORIZONTAL);
		else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
			setOrientation(VERTICAL);
		
		setGravity(Gravity.CENTER_HORIZONTAL);
		
		LayoutInflater.from(context).inflate(R.layout.yes_no_component, this);
	}

	@Override
	protected void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			setOrientation(VERTICAL);
		
		else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
			setOrientation(HORIZONTAL);
	}

}
