package com.concordia.etool;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.FrameLayout;

import com.concordia.etool.core.helpers.EToolApplication;
import com.concordia.etool.ui.QuestionComponent;

public class TellStoryTakePicture extends FragmentActivity {
	
	FrameLayout previewLayout;
	QuestionComponent questionComponent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.tell_story_pap);
		
		TakePictureFragment picFrag = new TakePictureFragment();
		getSupportFragmentManager().beginTransaction().add(picFrag, null).commit();
		
	}
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		EToolApplication.decrementNode(EToolApplication.currentNode);
		
	}
	
	

}