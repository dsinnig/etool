package com.concordia.etool;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.concordia.etool.core.PlayAudio;
import com.concordia.etool.core.helpers.EToolApplication;
import com.concordia.etool.graph.Node;
import com.concordia.etool.ui.QuestionComponent;

public class TellStoryPlayAudio extends Activity {

//	ImageButton playBtn;
	private String currentQuestion;
	QuestionComponent questionComponent;
	PlayAudio audioPlayer;
	
	long mStartTime;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tell_story_pa);

		audioPlayer = EToolApplication.getInstance(getApplicationContext()).getAudioPlayer();
		currentQuestion =  EToolApplication.getDirectory("questionsDir") + EToolApplication.currentNode.getAudio();
		audioPlayer.playAudio(currentQuestion);
		
		
		
		audioPlayer.getMediaPlayer().setOnCompletionListener(new OnCompletionListener() {
			
			@Override
			public void onCompletion(MediaPlayer mp) {
				moveToNextNode();
			}
		});
		questionComponent = (QuestionComponent) findViewById(R.id.questionComponent);
		questionComponent.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(audioPlayer.isPlaying())
					audioPlayer.resetAudio();
				audioPlayer.playAudio(currentQuestion);
			}
		});
		((TextView)questionComponent.findViewById(R.id.questionNo)).setText(Integer.toString(EToolApplication.currentNode.getId()));
		
//		playBtn = (ImageButton) findViewById(R.id.playBtn);
//		playBtn.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//
//			}
//		});
	}
	
	private void moveToNextNode()
	{
		//move to new new questions
		Intent newQuestionIntent = null;

		Node tempNode = EToolApplication.dialogGraph.getNextNode(EToolApplication.currentNode);

		if(tempNode != null)
		{
			switch (tempNode.getNodeType()) {

			case PLAY_AUDIO_RECORD:
				newQuestionIntent= new Intent(TellStoryPlayAudio.this,TellStoryActivity.class);
				break;

			case PLAY_AUDIO_TAKE_PICTURE:
				newQuestionIntent = new Intent(TellStoryPlayAudio.this, TellStoryTakePicture.class);
				break;

			case PLAY_AUDIO:
				newQuestionIntent = new Intent(TellStoryPlayAudio.this, TellStoryPlayAudio.class);
				break;
			case PLAY_AUDIO_KEYPAD_BRANCH:
				//
				break;
			case PLAY_VIDEO:
				//
				break;
			}
			EToolApplication.incrementNode(EToolApplication.currentNode);
		}

		else
			newQuestionIntent = new Intent(TellStoryPlayAudio.this, TellStoryEndActivity.class);

		startActivity(newQuestionIntent);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if(audioPlayer.isPlaying())
			audioPlayer.resetAudio();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		EToolApplication.decrementNode(EToolApplication.currentNode);
	}
	
	
	
}
