package com.concordia.etool.exception;

public class GraphException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public GraphException(String msg) {
		super(msg);
	}
	
}
