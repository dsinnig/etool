package com.concordia.etool.parsers;

public enum NodeType {
	PLAY_AUDIO_RECORD, PLAY_AUDIO_TAKE_PICTURE, PLAY_AUDIO_KEYPAD_BRANCH, PLAY_AUDIO, PLAY_VIDEO;
}
