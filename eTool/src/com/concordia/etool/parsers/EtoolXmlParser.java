package com.concordia.etool.parsers;

public abstract class EtoolXmlParser {
	
	final String xmlFileName;

	public EtoolXmlParser(String xmlFileName) {
		super();
		this.xmlFileName = xmlFileName;
	}
	
	abstract Object parse();
	
	abstract String toStringXMl();
}
