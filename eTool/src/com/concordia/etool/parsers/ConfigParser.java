package com.concordia.etool.parsers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import android.util.Xml;

import com.concordia.etool.core.helpers.EToolApplication;

public class ConfigParser extends AbstractConfigParser {

	XmlPullParser parser;

	public ConfigParser(String xmlFileName) {
		super(xmlFileName);

		try {

			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			parser = factory.newPullParser();

		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Object parse() {
		File xmlFile = new File(xmlFileName);

		if(xmlFile.exists())
			try {
				parser.setInput(new FileReader(xmlFile));
				int event = parser.getEventType();
				String tag;

				while(event!= XmlPullParser.END_DOCUMENT)
				{
					switch (event) {

					case XmlPullParser.START_TAG:

						tag = parser.getName();

						if(tag.equalsIgnoreCase(LIBRARY_DIR))
							EToolApplication.currentLibDirectory = parser.nextText();
						else if(tag.equalsIgnoreCase(INTRO_DIR))
							EToolApplication.currentIntroductionPromptsDirectory = parser.nextText();
						else if (tag.equalsIgnoreCase(NAVIGATION_DIR))
							EToolApplication.currentNavigationPromptsDirectory = parser.nextText();
						else if (tag.equalsIgnoreCase(QUESTIONS_DIR))
							EToolApplication.currentQuestionsPromptsDirectory = parser.nextText();
						break;

					case XmlPullParser.END_TAG:
						tag = parser.getName();
						if(tag.equalsIgnoreCase(SUB_ROOT))
							break;
						break;

					}
					event = parser.next();
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return null;
	}

	
	@Override
	public String toStringXMl() {
		XmlSerializer serializer = Xml.newSerializer();
		StringWriter writer = new StringWriter();
		try {
			serializer.setOutput(writer);
			serializer.startDocument("UTF-8", true);
			serializer.startTag("", ROOT);
			
			serializer.startTag("", SUB_ROOT);
			serializer.attribute("", ATTRIBUTE_NAME, EToolApplication.mainDirectory.substring(EToolApplication.mainDirectory.lastIndexOf(File.separator)));
			
			serializer.startTag("", LIBRARY_DIR);
			serializer.attribute("", ATTRIBUTE_NAME, EToolApplication.libDirectory);
			serializer.text(EToolApplication.currentLibDirectory);
			serializer.endTag("", LIBRARY_DIR);
			
			serializer.startTag("", PROMPTS_DIR);
			serializer.attribute("", ATTRIBUTE_NAME, EToolApplication.promptsDirectory);
			
			serializer.startTag("", INTRO_DIR);
			serializer.attribute("", ATTRIBUTE_NAME, EToolApplication.introductionPromptsDirectory);
			serializer.text(EToolApplication.currentIntroductionPromptsDirectory);
			serializer.endTag("", INTRO_DIR);
			
			serializer.startTag("", NAVIGATION_DIR);
			serializer.attribute("", ATTRIBUTE_NAME, EToolApplication.navigationPromptsDirectory);
			serializer.text(EToolApplication.currentNavigationPromptsDirectory);
			serializer.endTag("", NAVIGATION_DIR);
			
			serializer.startTag("", QUESTIONS_DIR);
			serializer.attribute("", ATTRIBUTE_NAME, EToolApplication.questionsPromptsDirectory);
			serializer.text(EToolApplication.currentQuestionsPromptsDirectory);
			serializer.endTag("", QUESTIONS_DIR);
			
			serializer.endTag("", PROMPTS_DIR);
			serializer.endTag("", SUB_ROOT);
			
			serializer.endTag("", ROOT);
			
			serializer.endDocument();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return writer.toString();
	}

}
