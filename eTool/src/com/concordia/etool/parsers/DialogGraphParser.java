package com.concordia.etool.parsers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import com.concordia.etool.graph.DialogGraph;
import com.concordia.etool.graph.Node;

public class DialogGraphParser extends AbstractDialogParser {

	XmlPullParser parser;

	public DialogGraphParser(String xmlFileName) {
		super(xmlFileName);

		try {
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			parser = factory.newPullParser();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
	}

	@Override
	public DialogGraph parse()
	{
		File xmlFile = new File(xmlFileName);
		DialogGraph dialogGraph = null;
		Node currentNode = null;

		if(xmlFile.exists())
			try {
				parser.setInput(new FileReader(xmlFile));
				int event = parser.getEventType();
				String tag;

				while(event!= XmlPullParser.END_DOCUMENT)
				{
					switch (event) {
					
					case XmlPullParser.START_DOCUMENT:
					
						dialogGraph = new DialogGraph(null);
						
						break;
						
					case XmlPullParser.START_TAG:
					
						tag = parser.getName();
						
						if(tag.equalsIgnoreCase(NODE))
						{
							currentNode = new Node( null, parser.getAttributeValue(null,"type") );
							currentNode.setId( Integer.parseInt ( parser.getAttributeValue(null, "id") ) );
						}
						else if(currentNode!=null)
						{
							if(tag.equalsIgnoreCase(AUDIO_FILE))
								currentNode.setAudio( parser.nextText() );
							
							else if (tag.equalsIgnoreCase(IMAGE_FILE))
								currentNode.setImage( parser.nextText() );
							
							else if (tag.equalsIgnoreCase(VIDEO_FILE))
								currentNode.setVideo( parser.nextText() );
							
							else if (tag.equalsIgnoreCase(SUCCESSOR))
								currentNode.setSuccessor( dialogGraph.get ( Integer.parseInt ( parser.nextText() ) ) );
							
						}
					
					break;

					case XmlPullParser.END_TAG:
						tag = parser.getName();
						if(tag.equalsIgnoreCase(NODE))
							dialogGraph.add(currentNode);
						break;

					}
					event = parser.next();
				}

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		return dialogGraph;
	}

	@Override
	String toStringXMl() {
		return null;
	}

}
