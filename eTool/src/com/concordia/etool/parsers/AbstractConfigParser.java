package com.concordia.etool.parsers;

public abstract class AbstractConfigParser extends EtoolXmlParser{

	static final String ROOT = "etool";
	static final String SUB_ROOT = "mainDir";
	static final String LIBRARY_DIR = "libDir";
	static final String PROMPTS_DIR = "promptsDir";
	static final String INTRO_DIR = "introDir";
	static final String NAVIGATION_DIR = "navDir";
	static final String QUESTIONS_DIR = "questionsDir";
	
	static final String ATTRIBUTE_NAME = "value";

	public AbstractConfigParser(String xmlFileName) {
		super(xmlFileName);
	}
	
	@Override
	abstract Object parse();
}

