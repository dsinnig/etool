package com.concordia.etool.parsers;

public interface NodeTypeXML {
	
	public String PLAY_AUDIO_RECORD = "PLAY_AUDIO_RECORD";
	public String PLAY_AUDIO_TAKE_PICTURE = "PLAY_AUDIO_TAKE_PICTURE";
	public String PLAY_AUDIO_KEYPAD_BRANCH = "PLAY_AUDIO_KEYPAD_BRANCH";
	public String PLAY_AUDIO = "PLAY_AUDIO";
	public String PLAY_VIDEO = "PLAY_VIDEO"; 
}
