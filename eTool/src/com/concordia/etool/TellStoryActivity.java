package com.concordia.etool;

import java.io.File;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.concordia.etool.core.OnPromptCompletion;
import com.concordia.etool.core.PlayAudio;
import com.concordia.etool.core.PromptPlayer;
import com.concordia.etool.core.RecordAudio;
import com.concordia.etool.core.helpers.EToolApplication;
import com.concordia.etool.core.helpers.Story;
import com.concordia.etool.graph.Node;
import com.concordia.etool.ui.QuestionComponent;
import com.concordia.etool.ui.YesNoComponent;

public class TellStoryActivity extends Activity implements OnClickListener, OnPromptCompletion{

	ImageButton startRecButton, stopRecButton, approveDiscard, disapproveDiscard;
	ProgressBar progress;

	RecordAudio recAudio;
	ImageView imgRecord;
	
	QuestionComponent questionComponent;

	PromptPlayer promptPlayer;
	PlayAudio audioPlayer;
	String currentQuestion,pathToRecord;

	LinearLayout recActionLayout;
	YesNoComponent yesNo;
	ImageButton btnApprove, btnDecline;
	Dialog playRecorded;
	UpdateProgressAsyncTask progressUpdater;

	Animation alphaAnim;
	long mStartTime;

	String[] timeout_prompts = {"PAR_playAudio_timeout.wav","PAR_stop_timeout.wav","PAR_cancel_timeout.wav"};
	String[] navigationPrompts = {"PAR_stop.wav","PAR_cancel.wav","PAR_cancel_stopsign.wav", "PAR_cancel_trashcan.wav"};
	String currentNavigationPrompt = navigationPrompts[0];
	String currentTimeoutPrompt = timeout_prompts[0];

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tell_story_par);

		//check if directory exists & create it if not
		pathToRecord = getDir(Integer.toString(Story.storyCount+1), Context.MODE_PRIVATE).getAbsolutePath();

		currentQuestion =  EToolApplication.getDirectory("questionsDir") + EToolApplication.currentNode.getAudio();
		pathToRecord += File.separator+EToolApplication.currentNode.getId() + ".3gp";

		setUpViews();

		//Set up media for the activity
		promptPlayer = EToolApplication.getInstance(getApplicationContext()).getPromptPlayer();
		audioPlayer = EToolApplication.getInstance(getApplicationContext()).getAudioPlayer();
		recAudio = EToolApplication.getInstance(getApplicationContext()).getAudioRecorder();

		audioPlayer.playAudio(currentQuestion);
		audioPlayer.getMediaPlayer().setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				audioPlayer.resetAudio();
				mStartTime = SystemClock.currentThreadTimeMillis();
				timeoutHandler.postDelayed(timeoutRunner, 10000);
			}
		});

		progress.setMax(RecordAudio.maxDuration);

		playRecorded.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {
				//dialog action buttons
				approveDiscard = (ImageButton) playRecorded.findViewById(R.id.discardApprove);
				disapproveDiscard = (ImageButton) playRecorded.findViewById(R.id.discardDecline);
				disapproveDiscard.setOnClickListener(TellStoryActivity.this);
				approveDiscard.setOnClickListener(TellStoryActivity.this);
			}
		});


	}


	public void startPormpts()
	{
		promptPlayer = EToolApplication.getInstance(getApplicationContext()).getPromptPlayer();
		promptPlayer.playPrompt(currentQuestion);

		promptPlayer.setOnPromptCompleteListener(new OnPromptCompletion() {

			@Override
			public void onComplete(PromptPlayer promptPlayer) {
				if(promptPlayer.isPlaying())
				{
					timeoutHandler.postDelayed(timeoutRunner, 10000);
				}
			}
		});
	}

	private void setUpViews()
	{
		questionComponent = (QuestionComponent) findViewById(R.id.questionComponent);
		((TextView)questionComponent.findViewById(R.id.questionNo)).setText(Integer.toString(EToolApplication.currentNode.getId()));
		startRecButton = (ImageButton) findViewById(R.id.actionRecBtn);
		stopRecButton = (ImageButton) findViewById(R.id.actionStopRecBtn);
		progress = (ProgressBar) findViewById(R.id.recordingProgress);
		imgRecord = (ImageView) findViewById(R.id.imageRecShow);
		btnApprove = (ImageButton) findViewById(R.id.imageApprove);
		btnDecline = (ImageButton) findViewById(R.id.imageDecline);
		recActionLayout = (LinearLayout) findViewById(R.id.actionRecLayout);
		yesNo = (YesNoComponent) findViewById(R.id.confirmRecording);

		//set up listeners for click events
		startRecButton.setOnClickListener(this);
		stopRecButton.setOnClickListener(this);
		questionComponent.setOnClickListener(this);
		btnApprove.setOnClickListener(this);
		btnDecline.setOnClickListener(this);

		//disable stop button
		stopRecButton.setEnabled(false);

		//set up dialog
		playRecorded = new Dialog(TellStoryActivity.this);
		playRecorded.setContentView(R.layout.record_confirm_dialog);
		playRecorded.setCancelable(true);



	}

	@Override
	public void onClick(View v) {

		if(promptPlayer.isPlaying())
			promptPlayer.stopPrompt();
		
		if(audioPlayer.isPlaying())
			audioPlayer.resetAudio();
		
		timeoutHandler.removeCallbacks(timeoutRunner);

		switch(v.getId())
		{

		case R.id.actionRecBtn:

			if(!recAudio.isRecording())
			{
				recAudio.record(pathToRecord);
				progressUpdater = new UpdateProgressAsyncTask();
				progressUpdater.execute();
				startAnimation();
				stopRecButton.setEnabled(true);
				startRecButton.setEnabled(false);
			}

			break;

		case R.id.actionStopRecBtn:

			if(recAudio.isRecording())
			{
				recAudio.stopRecording();
				progressUpdater.cancel(true);
				stopAnimation();
				currentNavigationPrompt = navigationPrompts[0];
				currentTimeoutPrompt = timeout_prompts[1];
				playNavigationPrompts();
				imgRecord.setImageResource(R.drawable.recording_green);
				recActionLayout.setVisibility(View.GONE);
				yesNo.setVisibility(View.VISIBLE);
				imgRecord.setOnClickListener(this);
			}

			break;

		case R.id.questionComponent:

			audioPlayer.playAudio(currentQuestion);

			break;

		case R.id.discardApprove:

			imgRecord.setImageResource(R.drawable.recording_transparent_line_white);
			imgRecord.setOnClickListener(null);
			progress.setProgress(0);
			currentNavigationPrompt = navigationPrompts[3];
			playNavigationPrompts();
			currentTimeoutPrompt = timeout_prompts[0];
			yesNo.setVisibility(View.GONE);
			recActionLayout.setVisibility(View.VISIBLE);
			startRecButton.setEnabled(true);
			stopRecButton.setEnabled(false);
			playRecorded.dismiss();
			
			break;

		case R.id.discardDecline:

			currentNavigationPrompt = navigationPrompts[2];
			playNavigationPrompts();
			playRecorded.dismiss();
			
			break;

		case R.id.imageApprove:

			//move to new new questions
			Intent newQuestionIntent = null;

			Node tempNode = EToolApplication.dialogGraph.getNextNode(EToolApplication.currentNode);

			if(tempNode != null)
			{
				switch (tempNode.getNodeType()) {

				case PLAY_AUDIO_RECORD:
					newQuestionIntent= new Intent(this,TellStoryActivity.class);
					break;

				case PLAY_AUDIO_TAKE_PICTURE:
					newQuestionIntent = new Intent(this, TellStoryTakePicture.class);
					break;

				case PLAY_AUDIO:
					newQuestionIntent = new Intent(this, TellStoryPlayAudio.class);
					break;
				case PLAY_AUDIO_KEYPAD_BRANCH:
					//
					break;
				case PLAY_VIDEO:
					//
					break;
				}
				EToolApplication.incrementNode(EToolApplication.currentNode);
			}

			else
				newQuestionIntent = new Intent(this, TellStoryEndActivity.class);

			startActivity(newQuestionIntent);

			break;

		case R.id.imageDecline:

			playRecorded.show();
			currentNavigationPrompt = navigationPrompts[1];
			currentTimeoutPrompt = timeout_prompts[2];
			playNavigationPrompts();

			break;

		case R.id.imageRecShow:

			if (audioPlayer.isPlaying())
				audioPlayer.resetAudio();
			audioPlayer.playAudio(pathToRecord);
			break;

		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(recAudio.isRecording())
		{
			recAudio.stopRecording();
			recAudio.resetRecorder();
		}

		if(promptPlayer.isPlaying())
			promptPlayer.stopPrompt();
		if(audioPlayer.isPlaying())
			audioPlayer.resetAudio();

		timeoutHandler.removeCallbacks(timeoutRunner);
	}

	public void startAnimation()
	{
		alphaAnim = new AlphaAnimation(1,0);
		alphaAnim.setDuration(500);
		alphaAnim.setRepeatCount(Animation.INFINITE);
		alphaAnim.setRepeatMode(Animation.REVERSE);
		imgRecord.startAnimation(alphaAnim);

	}

	public void stopAnimation()
	{
		imgRecord.clearAnimation();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		
		EToolApplication.decrementNode(EToolApplication.currentNode);
	}



	final Handler timeoutHandler = new Handler();
	Runnable timeoutRunner = new Runnable() {

		@Override
		public void run() {
			final long start = mStartTime;
			long millis = SystemClock.uptimeMillis() - start;
			int seconds = (int) (millis / 1000);
			int minutes = seconds / 60;
			seconds     = seconds % 60;

			playTimeOutPrompts();

			timeoutHandler.postAtTime(this,
					start + (((minutes * 60) + seconds + 1) * 10000));
		}
	};

	public class UpdateProgressAsyncTask extends AsyncTask<Void, Integer, Void>
	{

		int progress;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progress = 0;
		}

		@Override
		protected Void doInBackground(Void... params) {
			while(progress<RecordAudio.maxDuration && !isCancelled())
			{
				progress+= 100;
				publishProgress(progress);
				SystemClock.sleep(100);
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			TellStoryActivity.this.progress.setProgress(values[0]);
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			TellStoryActivity.this.progress.setProgress(RecordAudio.maxDuration);
		}

	}

	@Override
	public void onComplete(PromptPlayer promptPlayer) {

		timeoutHandler.removeCallbacks(timeoutRunner);
		mStartTime = SystemClock.currentThreadTimeMillis();
		timeoutHandler.postDelayed(timeoutRunner, 10000);
	}

	private void playNavigationPrompts() {

		promptPlayer.playPrompt(currentNavigationPrompt);
	}

	protected void playTimeOutPrompts() {
		if (promptPlayer.isPlaying()) 
			promptPlayer.stopPrompt();

		promptPlayer.playPrompt(currentTimeoutPrompt);
	}

}